package newutilities;


public class ProjectConstants {

    public static final String API_KEY_INTERCOM = "android_sdk-b62ed6729e673deb85a17eb189c72402349ef859";
    public static final String APP_ID_INTERCOM = "pt8zaakf";
    public static final String INTERCOM_PUSH_NOTIFICATION_PREFIX = "http://www.cohoapp.in/";
    public static final String INTENT_THROUGH_PUSH_NOTIFICATION = "is_through_pn";



    public static final String USER_ID = "USERID";
    public static final String USER_ID_TWO = "user_id";
    public static final String COHO_ID = "cohoid";
    public static final String USER_NAME = "USERNAME";
    public static final String USER_STATUS = "status";
    public static final String USER_STATUS_TWO = "STATUS";
    public static final String USER_IMAGE = "image";
    public static final String USER_MOBILE = "MOBILE";
    public static final String USER_PASSWORD = "PASSWORD";
    public static final String USER_TYPE = "USERTYPE";
    public static final String USER_COHO_SERVICE = "COHOSERVICE";
    public static final String USER_EMAIL_ID = "EMAIL";
    public static final String USER_RENT = "RENT";

}
