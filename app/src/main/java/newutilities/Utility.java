package newutilities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.List;

public class Utility {

    /**********************************
     * Integer
     ****************************************************************************/
    public static void saveIntValueInSharedPrefrence(Context context, String key, int value) {

        SharedPreferences pref = context.getSharedPreferences("theaterears", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(key, value);
        editor.commit();

    }

    public static int getIntValueFromSharedPrefernce(Context context, String key) {

        SharedPreferences prefs = context.getSharedPreferences("theaterears", Context.MODE_PRIVATE);
        int restoredInt = prefs.getInt(key, -1);
        return restoredInt;

    }

    /**********************************
     * Boolean
     ****************************************************************************/
    public static void saveBooleanValueInSharedPrefrence(Context context, String key, boolean value) {

        SharedPreferences pref = context.getSharedPreferences("theaterears", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key, value);
        editor.commit();

    }

    public static boolean getBooleanValueFromSharedPrefernce(Context context, String key) {

        SharedPreferences prefs = context.getSharedPreferences("theaterears", Context.MODE_PRIVATE);
        boolean restoredBoolean = prefs.getBoolean(key, false);
        return restoredBoolean;

    }

    /**********************************
     * Long
     ****************************************************************************/
    public static void saveLongValueInSharedPrefrence(Context context, String key, long value) {

        SharedPreferences pref = context.getSharedPreferences("theaterears", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putLong(key, value);
        editor.commit();

    }

    public static long getLongValueFromSharedPrefernce(Context context, String key) {

        SharedPreferences prefs = context.getSharedPreferences("theaterears", Context.MODE_PRIVATE);
        long restoredLong = prefs.getLong(key, 0);
        return restoredLong;

    }

    /**********************************
     * Float
     ****************************************************************************/
    public static void saveFloatValueInSharedPrefrence(Context context, String key, float value) {

        SharedPreferences pref = context.getSharedPreferences("theaterears", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putFloat(key, value);
        editor.commit();

    }


    public static float getFloatValueFromSharedPrefernce(Context context, String key) {

        SharedPreferences prefs = context.getSharedPreferences("theaterears", Context.MODE_PRIVATE);
        float restoredLong = prefs.getFloat(key, 0.0f);
        return restoredLong;

    }


    /**********************************
     * String
     ****************************************************************************/
    public static String getStringValueFromSharedPrefernce(Context context, String key) {

        SharedPreferences prefs = context.getSharedPreferences("theaterears", Context.MODE_PRIVATE);
        String restoredText = prefs.getString(key, null);
        return restoredText;

    }

    public static void saveStringValueInSharedPrefrence(Context context, String key, String value) {

        SharedPreferences pref = context.getSharedPreferences("theaterears",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.commit();

    }

    public static boolean isAvailable(Context ctx, Intent intent) {
        final PackageManager mgr = ctx.getPackageManager();
        List<ResolveInfo> list =
                mgr.queryIntentActivities(intent,
                        PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    /**
     * method to check if mobile is connected to wifi or not
     *
     * @param context - context of calling activity
     * @return boolean - if mobile is connected to mobile data internet or not
     */
    public static boolean isWifiConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;
        if (connectivityManager != null) {
            networkInfo =
                    connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        }
        return networkInfo == null ? false : networkInfo.isConnected();
    }

    /**
     * method to check if mobile is connected to mobile data internet or not
     *
     * @param context - context of calling activity
     * @return boolean - if mobile is connected to mobile data internet or not
     */
    public static boolean isMobileConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);


        NetworkInfo networkInfo = null;
        if (connectivityManager != null) {
            networkInfo =
                    connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        }
        return networkInfo == null ? false : networkInfo.isConnected();
    }
}
