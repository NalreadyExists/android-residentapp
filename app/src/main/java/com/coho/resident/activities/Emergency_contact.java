package com.coho.resident.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.coho.resident.R;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class Emergency_contact extends AppCompatActivity {

    EditText emergency1, emergency2, name1, name2, relation1, relation2, address1, address2;
    SharedPreferences sharedPreferences;
    String userId;


    @Override
    protected void onResume() {
        super.onResume();

        try {
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            userId = sharedPreferences.getString("USERID", "");
            Ion.with(getApplicationContext())
                    .load("http://bats.coho.in/webservice/cohoresident_contact_emergency_view?userid=")
                    .setMultipartParameter("userid", userId)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {

                            JSONObject jsonObj;
                            try {
                                jsonObj = new JSONObject(result.toString());
                                if (jsonObj.getString("status").equals("true")) {

                                    jsonObj=new JSONObject(jsonObj.getString("data").toString());
                                    name1.setText(jsonObj.getString("coho_guardiansname"));
                                    name2.setText(jsonObj.getString("coho_guardiansname_sec"));
                                    emergency1.setText(jsonObj.getString("coho_guardiansmobile"));
                                    emergency2.setText(jsonObj.getString("coho_guardiansmobile_sec"));
                                    relation1.setText(jsonObj.getString("coho_relationone"));
                                    relation2.setText(jsonObj.getString("coho_relationtwo"));
                                    address1.setText(jsonObj.getString("coho_relativeaddressone"));
                                    address2.setText(jsonObj.getString("coho_relativeaddresstwo"));
                                } else {
                                    Toast.makeText(getApplicationContext(), result.toString(), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }


                        }
                    }).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_contact);
        emergency1 = (EditText) findViewById(R.id.emergency1);
        emergency2 = (EditText) findViewById(R.id.emergency2);
        name1 = (EditText) findViewById(R.id.emergency_contact1);
        name2 = (EditText) findViewById(R.id.emergency_contact2);
        relation1 = (EditText) findViewById(R.id.relation);
        relation2 = (EditText) findViewById(R.id.relation2);
        address1 = (EditText) findViewById(R.id.address);
        address2 = (EditText) findViewById(R.id.address2);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Button upload = (Button) findViewById(R.id.up);

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (emergency1.getText().toString().length() < 10 || name1.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Please enter valid credentials for emergency contact 1", Toast.LENGTH_SHORT).show();
                } else if (emergency2.getText().toString().length() < 10 || name2.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Please enter valid credentials for emergency contact 2", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        userId = sharedPreferences.getString("USERID", "");
                        Ion.with(getApplicationContext())
                                .load("http://bats.coho.in/webservice/cohoresident_contact_emergency")
                                .setMultipartParameter("userid", userId)
                                .setMultipartParameter("coho_guardiansname", name1.getText().toString())
                                .setMultipartParameter("coho_guardiansmobile", emergency1.getText().toString())
                                .setMultipartParameter("coho_guardiansmobile_sec", emergency2.getText().toString())
                                .setMultipartParameter("coho_guardiansname_sec", name2.getText().toString())
                                .setMultipartParameter("coho_relationone", relation1.getText().toString())
                                .setMultipartParameter("coho_relativeaddressone", address1.getText().toString())
                                .setMultipartParameter("coho_relationtwo", relation2.getText().toString())
                                .setMultipartParameter("coho_relativeaddresstwo", address2.getText().toString())
                                .asJsonObject()
                                .setCallback(new FutureCallback<JsonObject>() {
                                    @Override
                                    public void onCompleted(Exception e, JsonObject result) {

                                        JSONObject jsonObj;
                                        try {
                                            jsonObj = new JSONObject(result.toString());
                                            if (jsonObj.getString("status").equals("true")) {
                                                Toast.makeText(getApplicationContext(), "Contact details uploaded succesfully", Toast.LENGTH_SHORT).show();
                                                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Emergency_contact.this);
                                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                                editor.putString("image", jsonObj.getString("card_pic"));
                                                editor.commit();
                                                Intent main = new Intent(getApplicationContext(), Splash.class);
                                                startActivity(main);
                                                finish();
                                            } else {
                                                Toast.makeText(getApplicationContext(), result.toString(), Toast.LENGTH_SHORT).show();
                                            }
                                        } catch (JSONException e1) {
                                            e1.printStackTrace();
                                        }


                                    }
                                }).get();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                }

            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
