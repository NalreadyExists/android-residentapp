package com.coho.resident.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.coho.resident.R;
import com.coho.resident.Utilities.Constants;
import com.coho.resident.Utilities.NotificationListAdapter;
import com.google.android.gms.analytics.Tracker;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class Complaints2 extends AppCompatActivity {

    ArrayList<String> heading = new ArrayList<>();
    ArrayList<String> imageAr = new ArrayList<>();
    ArrayList<String> descAr = new ArrayList<>();
    ArrayList<String> dateAr = new ArrayList<>();
    ArrayList<JSONObject> jsonObjects = new ArrayList<>();
    Tracker mTracker;

    JSONObject json;
    String url, userId;
    JSONArray jsonArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_complaints2);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();

        Constants.sendScreenImageName(mTracker, "Nottifications");


        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        userId = sharedPreferences.getString("USERID", "");



        if (imageAr.size() == 0) {

            try {

                Ion.with(getApplicationContext())
                        .load(Constants.COHO_NOTIFICATION2)
                        .setBodyParameter("user_id", userId)
                        .asString()
                        .setCallback(new FutureCallback<String>() {
                            @Override
                            public void onCompleted(Exception e, String result) {
                                // do stuff with the result or error
                                JSONObject jsonObj=null;
                                try {
                                    if (result != null) {
                                        jsonObj = new JSONObject(result);
                                    }
                                    if (jsonObj.getString("status").equals("true")) {
                                        JSONArray jsonObj1 = jsonObj.getJSONArray("data");
                                       /* JSONArray imageJson = jsonObj.getJSONArray("image");
                                        JSONArray varJson = jsonObj.getJSONArray("description");
                                        JSONArray headingJson = jsonObj.getJSONArray("title");*/

                                        for (int i = 0; i < jsonObj1.length(); i++) {
                                            JSONObject j = jsonObj1.getJSONObject(i);
                                            jsonObjects.add(j);
                                            imageAr.add(j.getString("image"));
                                            descAr.add(j.getString("description"));
                                            heading.add(j.getString("title"));
                                            dateAr.add(j.getString("posted_date"));
                                        }

                                        imageAr.size();
                                        descAr.size();
                                    }
                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }


                            }
                        }).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

        }

        NotificationListAdapter adapter = new NotificationListAdapter(this, imageAr, descAr, heading, dateAr, jsonObjects);
        ListView lv = (ListView) findViewById(R.id.list);
        lv.setAdapter(adapter);

        LinearLayout no = (LinearLayout) findViewById(R.id.nodata);
        lv.setEmptyView(no);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    final int position, long id) {


                JSONObject j = jsonObjects.get(position);

                Intent in = new Intent(getApplicationContext(),
                        DetailedNotification.class);
                in.putExtra("product_object", j.toString());
                in.putExtra("check", "ok");

                startActivity(in);

            }
        });

        Button complaint = (Button) findViewById(R.id.logComplaint);

        complaint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent oldCompalints = new Intent(getApplication(), com.coho.resident.activities.Complaints.class);
                startActivity(oldCompalints);
            }
        });
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
