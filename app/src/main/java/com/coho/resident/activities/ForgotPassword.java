package com.coho.resident.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.coho.resident.R;
import com.coho.resident.Utilities.Constants;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

public class ForgotPassword extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (!(getSupportActionBar() == null)) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }
        final EditText mobile = (EditText) findViewById(R.id.mobile);
        Button submit = (Button) findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mobile.getText().toString().length() < 10) {
                    Toast.makeText(getApplicationContext(), "Invalid mobile number", Toast.LENGTH_SHORT).show();
                } else {
                    Ion.with(getApplicationContext())
                            .load(Constants.COHO_FORGOT_PASSWORD)
                            .setBodyParameter("mobile", mobile.getText().toString())
                            .asString()
                            .setCallback(new FutureCallback<String>() {
                                @Override
                                public void onCompleted(Exception e, String result) {
                                    // do stuff with the result or error
                                    JSONObject jsonObj;
                                    try {
                                        jsonObj = new JSONObject(result);
                                        if (jsonObj.getString("status").equals("true")) {
                                            String email = jsonObj.getString("email");
                                            Toast.makeText(getApplicationContext(), "Your password has been e-mailed to:" + email + "\n For any urgent queries, feel free to reach us at CoHo concierge: 9999567617  ", Toast.LENGTH_SHORT).show();
                                            Intent login = new Intent(getApplicationContext(), Login.class);
                                            startActivity(login);
                                            finish();
                                        } else {
                                            Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }


                                }
                            });
                }
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
