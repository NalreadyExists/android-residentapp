package com.coho.resident.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.coho.resident.R;
import com.coho.resident.Services.ServiceHandler;
import com.coho.resident.Utilities.Constants;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import io.intercom.android.sdk.Intercom;
import newutilities.ProjectConstants;
import newutilities.Utility;

/**
 * Created by Samar on 8/27/2015.
 */
public class Splash extends Activity {
    String mobileNum, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        //checking if intent is via push notification or not
        Intent intent = getIntent();
        if (intent.getData() != null) {
            Toast.makeText(this,"Intercom data received : \n\n" + intent.getData(),Toast.LENGTH_LONG).show();
            if (!(TextUtils.isEmpty(intent.getData().toString()))) {
                if (intent.getData().toString().startsWith("coho_app_")) {
                    String action = intent.getAction();

                    //String action = Intent.ACTION_VIEW;
                    String data = intent.getData().toString();
                    intent.putExtra(ProjectConstants.INTENT_THROUGH_PUSH_NOTIFICATION , true);
                    data = ProjectConstants.INTERCOM_PUSH_NOTIFICATION_PREFIX + data;
                    if(Utility.isAvailable(Splash.this , intent)) {
                        Splash.this.startActivity(new Intent(action, Uri.parse(data)));
                        Splash.this.finish();
                        return;
                    }
                    //Intercom.client().openGCMMessage(new Intent(action, Uri.parse(data)));

                }
            }
        }

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mobileNum = sharedPreferences.getString("MOBILE", "default");
        password = sharedPreferences.getString("PASSWORD", "default");

        if (mobileNum.equals("default") || password.equals("default")) {
            Thread timer = new Thread() {
                public void run() {
                    try {
                        sleep(2000);

                    } catch (InterruptedException e) {
                        e.printStackTrace();

                    } finally {
                        Intent login = new Intent(Splash.this, Login.class);
                        startActivity(login);

                    }

                }
            };
            timer.start();
        } else {
            postData();
        }

    }

    public void postData() {
        Log.e("", "");
        Boolean is = Constants.isInternetAvailable(getApplicationContext());

        if (Constants.isInternetAvailable(getApplicationContext())) {

            Ion.with(getApplicationContext())
                    .load(Constants.COHO_LOGIN)
                    .setBodyParameter("mobile", mobileNum)
                    .setBodyParameter("password", password)
                    .asString()
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String result) {
                            // do stuff with the result or error
                            JSONObject jsonObj;
                            try {
                                jsonObj = new JSONObject(result);
                                if (jsonObj.getString("status").equals("success")) {
                                    Intent nextIntent = new Intent(Splash.this, MainActivity.class);
                                    startActivity(nextIntent);
                                    finish();


                                } else {
                                    Intent nextScreenTntent = new Intent(Splash.this, Login.class);
                                    startActivity(nextScreenTntent);
                                    finish();
                                }
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }


                        }
                    });


        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(Splash.this);
            alertDialog.create();
            alertDialog
                    .setMessage("No Internet Connection,Do you want to Exit");
            alertDialog.setCancelable(false);
            alertDialog.setPositiveButton("Yes",
                    new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {

                            finish();

                        }
                    });
            alertDialog.setNegativeButton("No",
                    new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }


}
