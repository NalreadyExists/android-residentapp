package com.coho.resident.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.coho.resident.R;
import com.coho.resident.Utilities.Constants;
import com.coho.resident.Utilities.DisplayOfferListAdapter;
import com.google.android.gms.analytics.Tracker;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;

public class DisplayOffers extends AppCompatActivity {

    String[] phoneArray;
    ArrayList<String> imgUrlList = new ArrayList<>();
    ArrayList<String> key = new ArrayList<>();
    ArrayList<String> link = new ArrayList<>();
    Activity activity;
    Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_offers);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();

        Constants.sendScreenImageName(mTracker, "Display offers");


        activity = this;

        Intent intent = getIntent();
        final int pos = intent.getIntExtra("POSITION", 0);
        String var = intent.getStringExtra("var");

        getSupportActionBar().setTitle(var);

        String url = Constants.COHO_FLYER_NE + var;

        Ion.with(this)
                .load(url)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        JSONObject jsonObj;
                        try {
                            jsonObj = new JSONObject(result);
                            Log.e("json",result);
                            if (jsonObj.getString("status").equals("success")) {

                                int length = jsonObj.length() - 2;
                                phoneArray = new String[length];

                                for (int i = 0; i < length; i++) {
                                    String imgUrl = jsonObj.getJSONObject(String.valueOf(i)).getString("flyer");
                                    String mobileNum = jsonObj.getJSONObject(String.valueOf(i)).getString("mobile");
                                    String str = jsonObj.getJSONObject(String.valueOf(i)).getString("id");
                                    String link1 = jsonObj.getJSONObject(String.valueOf(i)).getString("link")+Constants.COHO_UTM_RESIDENT;

                                    phoneArray[i] = URLDecoder.decode(mobileNum, "UTF-8");
                                    link.add(link1);
                                    imgUrlList.add(imgUrl);
                                    key.add(str);

                                }
                                DisplayOfferListAdapter adapter = new DisplayOfferListAdapter(activity, phoneArray, imgUrlList, key, pos, link);
                                ListView lv = (ListView) findViewById(R.id.offer_listview);
                                lv.setAdapter(adapter);
                            } else {
                                Toast.makeText(DisplayOffers.this, "No Offers available", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        } catch (UnsupportedEncodingException e1) {
                            e1.printStackTrace();
                        }
                    }
                });


    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
