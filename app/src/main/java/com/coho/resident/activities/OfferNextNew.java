package com.coho.resident.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coho.resident.R;
import com.coho.resident.Utilities.Constants;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import newutilities.ProjectConstants;

public class OfferNextNew extends AppCompatActivity {

    String phoneNum, key;
    JSONObject j;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_offer);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView about = (TextView) findViewById(R.id.about);
        TextView howTo = (TextView) findViewById(R.id.how_to_avail);
        TextView offer = (TextView) findViewById(R.id.offer);
        TextView location = (TextView) findViewById(R.id.location);
        Button call =(Button) findViewById(R.id.call);
        Button website =(Button) findViewById(R.id.website_button);
//        TextView mobile = (TextView) findViewById(R.id.number);






        ImageView img = (ImageView) findViewById(R.id.logo);
        img.setScaleType(ImageView.ScaleType.FIT_XY);

        phoneNum = getIntent().getStringExtra("json");

        boolean isViaPushNotification = getIntent().getBooleanExtra(ProjectConstants.INTENT_THROUGH_PUSH_NOTIFICATION , false);
        if(isViaPushNotification) {
            call.setVisibility(View.INVISIBLE);
            website.setVisibility(View.INVISIBLE);
        }

        if(phoneNum != null && phoneNum.length() > 0) {
            Log.e("JSON",phoneNum);

            try {
                j= new JSONObject(phoneNum);

                Ion.with(img)
                        .error(R.drawable.app_icon)
                        .load(j.getString("logo"));

                about.setText(j.getString("about"));
                howTo.setText(j.getString("how_to_avail"));
                offer.setText(j.getString("offer"));
                location.setText(j.getString("location"));

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                try {
                    if(!TextUtils.isEmpty(j.getString("contact"))) {
                        intent.setData(Uri.parse("tel:" + j.getString("contact")));
                        startActivity(intent);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_VIEW);
                try {
                    if(!TextUtils.isEmpty(j.getString("website"))) {
                        intent.setData(Uri.parse(j.getString("website") + Constants.COHO_UTM_RESIDENT));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                startActivity(intent);

            }
        });





    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
