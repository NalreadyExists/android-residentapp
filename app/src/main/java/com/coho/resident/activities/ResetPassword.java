package com.coho.resident.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.coho.resident.R;
import com.coho.resident.Utilities.Constants;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;


public class ResetPassword extends AppCompatActivity {
    String userId;
    EditText op, np, cp;
    JSONObject jsonObj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        op = (EditText) findViewById(R.id.old_password);
        np = (EditText) findViewById(R.id.new_password);
        cp = (EditText) findViewById(R.id.confirm_password);
        op.setTransformationMethod(new PasswordTransformationMethod());
        np.setTransformationMethod(new PasswordTransformationMethod());
        cp.setTransformationMethod(new PasswordTransformationMethod());


        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        userId = sharedPreferences.getString("USERID", "");

        Button submit;
        submit = (Button) findViewById(R.id.reset_button);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String o, n, c;
                o = op.getText().toString();
                n = np.getText().toString();
                c = cp.getText().toString();
                String url = Constants.COHO_FORGOT_PASSWORD + "?user_id=" + userId + "&old_pwd=" + o + "&new_pwd=" + n +
                        "&confirm_pwd=" + c;


                Ion.with(getApplicationContext())
                        .load(Constants.COHO_RESET_PASSWORD)
                        .setBodyParameter("user_id", userId)
                        .setBodyParameter("old_pwd", o)
                        .setBodyParameter("new_pwd", n)
                        .setBodyParameter("confirm_pwd", c)
                        .asString()
                        .setCallback(new FutureCallback<String>() {
                            @Override
                            public void onCompleted(Exception e, String result) {
                                // do stuff with the result or error

                                try {
                                    jsonObj = new JSONObject(result);
                                    /*JSONParser parser = new JSONParser();
                                    JSONObject j = parser.getJSONData(url);*/
                                    try {
                                        if (jsonObj.getString("status").equals("success")) {
                                            Toast.makeText(ResetPassword.this, "Password changed successfully", Toast.LENGTH_SHORT).show();
                                            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ResetPassword.this);
                                            SharedPreferences.Editor editor = sharedPreferences.edit();
                                            editor.putString("PASSWORD",c);
                                            editor.commit();
                                            Intent nextScreenTntent = new Intent(ResetPassword.this, Splash.class);
                                            ResetPassword.this.finish();
                                            startActivity(nextScreenTntent);

                                        } else {
                                            String msg = jsonObj.getString("message");
                                            Toast.makeText(ResetPassword.this, msg, Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                    }

                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }


                            }
                        });




            }
        });


    }


    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        return true;
    }


}
