package com.coho.resident.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.coho.resident.R;
import com.coho.resident.Utilities.Constants;
import com.coho.resident.Utilities.NotificationListAdapter;
import com.coho.resident.Utilities.RentDetailsAdapter;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class Payments extends AppCompatActivity {

    ArrayList<JSONObject> jsonObjects = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_payments);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        /*
        {
        "data":{"1":{"name":"eYum","image":"http:\/\/bats.coho.in\/assets\/systemimages\/zocaloservices\/14461227121912.png","title":"Gym Membership in Gurgaon","desc":"lelo lelo lelo","amt":1500},"2":{"name":"eYum","image":"http:\/\/bats.coho.in\/assets\/systemimages\/zocaloservices\/14461227121912.png","title":"Gym membership in saket","desc":"lelo lelo lelo gold gym me lelo","amt":2000},"3":{"name":"PlankApp","image":"http:\/\/bats.coho.in\/assets\/systemimages\/zocaloservices\/14445806944939.png","title":"Dinner Subscription from PlankApp","desc":"dinner subscription from plank app for just Rs. 2500. Will have 2 sabzi, 4 Chapatis, Rice, Curd & a Sweet","amt":2500}},
        "message":"SUBSCRIPTION as follows.",
        "status":"true"
        }
         */

        try {

            SharedPreferences sharedPreferences;
            String userId;

            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            userId = sharedPreferences.getString("USERID", "");

            Ion.with(getApplicationContext())
                    .load(Constants.COHO_RENT_DETAILS)
                    .setBodyParameter("user_id", userId)
                    .asString()
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String result) {
                            // do stuff with the result or error
                            JSONObject jsonObj = null;
                            try {
                                if (result != null) {
                                    jsonObj = new JSONObject(result);
                                }
                                if (jsonObj.getString("status").equals("true")) {
                                    JSONObject jsonObj1 = jsonObj.getJSONObject("data");
                                       /* JSONArray imageJson = jsonObj.getJSONArray("image");
                                        JSONArray varJson = jsonObj.getJSONArray("description");
                                        JSONArray headingJson = jsonObj.getJSONArray("title");*/

                                    for (int i = 1; i <= jsonObj1.length(); i++) {
                                        JSONObject j = (JSONObject) jsonObj1.get(Integer.toString(i));
                                        jsonObjects.add(j);
                                    }

                                }
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }


                        }
                    }).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        RentDetailsAdapter rent = new RentDetailsAdapter(this, jsonObjects);
        ListView lv = (ListView) findViewById(R.id.payment_list);
        lv.setAdapter(rent);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    final int position, long id) {


                JSONObject j = jsonObjects.get(position);

                Intent in = new Intent(getApplicationContext(),
                        PaymentDetail.class);
                in.putExtra("json", j.toString());
                in.putExtra("check", "ok");

                startActivity(in);

            }
        });


    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
