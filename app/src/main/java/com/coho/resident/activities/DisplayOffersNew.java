package com.coho.resident.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.coho.resident.R;
import com.coho.resident.Utilities.Constants;
import com.coho.resident.Utilities.DisplayOfferListAdapter;
import com.coho.resident.Utilities.DisplayOfferListAdapterNew;
import com.google.android.gms.analytics.Tracker;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;

public class DisplayOffersNew extends AppCompatActivity {


    ArrayList<JSONObject> about = new ArrayList<>();
    Activity activity;
    JSONArray jsonObj1;
    Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_offers);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();

        Constants.sendScreenImageName(mTracker, "Display offers");


        activity = this;

        Intent intent = getIntent();
        final int pos = intent.getIntExtra("POSITION", 0);
        String var = intent.getStringExtra("var");

        getSupportActionBar().setTitle(var);

        String url = Constants.COHO_FLYER_NEW1;

        Ion.with(this)
                .load(url)
                .setBodyParameter("app_type","resident")
                .setBodyParameter("category",var)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        JSONObject jsonObj;
                        try {
                            jsonObj = new JSONObject(result);
                            Log.e("json2",result);
                            if (jsonObj.getString("status").equals("true")) {

                                jsonObj1= jsonObj.getJSONArray("data");
                                int length = jsonObj1.length();

                                for (int i = 0; i < length; i++) {
                                    about.add(jsonObj1.getJSONObject(i));
                                }

                                DisplayOfferListAdapterNew adapter = new DisplayOfferListAdapterNew(activity, pos, about);
                                ListView lv = (ListView) findViewById(R.id.offer_listview);
                                lv.setAdapter(adapter);

                               /* lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view,
                                                            final int position, long id) {


                                        Intent intent = new Intent(getApplicationContext(), OfferNextNew.class);
                                        intent.putExtra("json", about.get(pos).toString());
                                        startActivity(intent);

                                    }
                                });
*/
                            } else {
                                Toast.makeText(DisplayOffersNew.this, "No Offers available", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }
                });


    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
