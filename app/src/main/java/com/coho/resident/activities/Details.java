package com.coho.resident.activities;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.coho.resident.R;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class Details extends AppCompatActivity {

    TextView name,dob,mobile,company,address,location,occupancy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SharedPreferences sharedPreferences;
        String userId;

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        userId = sharedPreferences.getString("USERID", "");

        name =(TextView) findViewById(R.id.name);
        dob = (TextView) findViewById(R.id.dob);
        mobile = (TextView) findViewById(R.id.mobile);
        company=(TextView) findViewById(R.id.company);
        address=(TextView) findViewById(R.id.address);
        location=(TextView) findViewById(R.id.location);
        occupancy=(TextView) findViewById(R.id.occupancy);

        try {
            Ion.with(getApplicationContext())
                    .load("http://www.coho.in/webservice/cohoresident_personal_details")
                    .setMultipartParameter("userid", userId)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {

                            JSONObject jsonObj;
                            try {
                                jsonObj = new JSONObject(result.toString());
                                if (jsonObj.getString("status").equals("true")) {

                                    jsonObj = jsonObj.getJSONObject("data");
                                    name.setText(jsonObj.getString("name"));
                                    dob.setText(jsonObj.getString("dob"));
                                    mobile.setText(jsonObj.getString("mobile"));
                                    company.setText(jsonObj.getString("company"));
                                    address.setText(jsonObj.getString("address"));
                                    location.setText(jsonObj.getString("landmark"));
                                    occupancy.setText(jsonObj.getString("occupancy"));


                                } else {
                                    Toast.makeText(getApplicationContext(), result.toString(), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }


                        }
                    }).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
