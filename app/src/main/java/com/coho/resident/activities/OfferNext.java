package com.coho.resident.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coho.resident.R;
import com.koushikdutta.ion.Ion;

public class OfferNext extends AppCompatActivity {

    String phoneNum, key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_next);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);




        phoneNum = getIntent().getStringExtra("phone");
        key = getIntent().getStringExtra("key");

        ImageView img = (ImageView) findViewById(R.id.flyer_full);
        img.setScaleType(ImageView.ScaleType.FIT_XY);

        Ion.with(img)
                .error(R.drawable.app_icon)
                .load(key);
        Button btn = (Button) findViewById(R.id.call_button);

        TextView txt = (TextView) findViewById(R.id.phone_num);
        txt.setText(phoneNum);

        LinearLayout ll = (LinearLayout) findViewById(R.id.call_ll);
        ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + phoneNum));
                startActivity(intent);
            }
        });



    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
