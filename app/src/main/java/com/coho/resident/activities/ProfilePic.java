package com.coho.resident.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.coho.resident.R;
import com.coho.resident.Utilities.Constants;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class ProfilePic extends AppCompatActivity {


    final int REQUEST_IMAGE_CAPTURE = 1, SELECT_FILE = 2;
    ImageView image, image1, image2;
    EditText emergency1, emergency2, name1, name2, relation1, relation2, address1, address2;
    File destination_temp, destination, destination1, destination2;
    SharedPreferences sharedPreferences;
    String userId;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            try {
                if (extras.get("data") != null) {
                    Bitmap imageBitmap = (Bitmap) extras.get("data");
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    if (imageBitmap != null) {
                        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                    }
                    destination_temp = new File(Environment.getExternalStorageDirectory(),
                            System.currentTimeMillis() + ".jpg");
                    FileOutputStream fo;
                    try {
                        destination_temp.createNewFile();
                        fo = new FileOutputStream(destination_temp);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (requestCode == 1) {
                        image.setImageBitmap(imageBitmap);
                        destination = destination_temp;
                    }
                }


            } catch (Exception e) {


            }
        }

        if (requestCode == SELECT_FILE && resultCode == RESULT_OK) {
            try {
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                CursorLoader cursorLoader = new CursorLoader(this, selectedImageUri, projection, null, null,
                        null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                String selectedImagePath = cursor.getString(column_index);
                Bitmap bm;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(selectedImagePath, options);
                final int REQUIRED_SIZE = 200;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bm = BitmapFactory.decodeFile(selectedImagePath, options);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                destination_temp = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");
                FileOutputStream fo;
                try {
                    destination_temp.createNewFile();
                    fo = new FileOutputStream(destination_temp);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (requestCode == 2) {
                    destination = destination_temp;
                    image.setImageBitmap(bm);
                }
            } catch (Exception e) {

            }
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_pic);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Button camera = (Button) findViewById(R.id.camera);
        image = (ImageView) findViewById(R.id.image);
        Button upload = (Button) findViewById(R.id.up);

        try {
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            userId = sharedPreferences.getString("USERID", "");
            Ion.with(getApplicationContext())
                    .load("http://www.coho.in/webservice/cohoresident_profilepic?userid=")
                    .setMultipartParameter("userid", userId)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {

                            JSONObject jsonObj;
                            try {
                                jsonObj = new JSONObject(result.toString());
                                if (jsonObj.getString("status").equals("true")) {
                                    String image_url1;
                                    image_url1 = jsonObj.getString("imageurl");
                                    Ion.with(image)
                                            .load(image_url1);
                                } else {
                                    Toast.makeText(getApplicationContext(), result.toString(), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }


                        }
                    }).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(1);
            }
        });

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(1);
            }
        });

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        final String userId = sharedPreferences.getString("USERID", "");

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {
                    if (destination == null) {
                        Toast.makeText(getApplicationContext(), "Please select an image first", Toast.LENGTH_SHORT).show();
                    } else {
                        Ion.with(getApplicationContext())
                                .load("http://www.coho.in/webservice/cohoresident_upload_profilepic")
                                .setMultipartParameter("userid", userId)
                                .setMultipartFile("userimage", "image/jpg", destination)
                                .asJsonObject()
                                .setCallback(new FutureCallback<JsonObject>() {
                                    @Override
                                    public void onCompleted(Exception e, JsonObject result) {

                                        JSONObject jsonObj;
                                        try {
                                            jsonObj = new JSONObject(result.toString());
                                            if (jsonObj.getString("status").equals("true")) {
                                                Toast.makeText(getApplicationContext(), "Profile pic uploaded succesfully", Toast.LENGTH_SHORT).show();
                                                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ProfilePic.this);
                                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                                editor.putString("image", jsonObj.getString("imageurl"));
                                                editor.commit();
                                               /* Intent main = new Intent(getApplicationContext(), Splash.class);
                                                startActivity(main);
                                                finish();*/
                                            } else {
                                                Toast.makeText(getApplicationContext(), result.toString(), Toast.LENGTH_SHORT).show();
                                            }
                                        } catch (JSONException e1) {
                                            e1.printStackTrace();
                                        }


                                    }
                                }).get();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }


            }
        });

    }

    private void selectImage(final int btn) {
        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(ProfilePic.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        if (btn == 1)
                            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    {
                        if (btn == 1)
                            startActivityForResult(
                                    Intent.createChooser(intent, "Select File"),
                                    SELECT_FILE);
                    }
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
