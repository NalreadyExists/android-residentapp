package com.coho.resident.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.coho.resident.R;

public class Docs extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_docs);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        LinearLayout docs,profile,emer,details,reset;
        docs =(LinearLayout) findViewById(R.id.docs);
        profile =(LinearLayout) findViewById(R.id.profile_pic);
        emer =(LinearLayout) findViewById(R.id.emergency_contact);
        details =(LinearLayout) findViewById(R.id.details);
        reset = (LinearLayout) findViewById(R.id.reset_password);

        docs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent kyc = new Intent(Docs.this, KYC.class);
                startActivity(kyc);

            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent kyc = new Intent(Docs.this, ProfilePic.class);
                startActivity(kyc);
            }
        });

        emer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent kyc = new Intent(Docs.this, Emergency_contact.class);
                startActivity(kyc);
            }
        });

        details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent kyc = new Intent(Docs.this, Details.class);
                startActivity(kyc);
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent kyc = new Intent(Docs.this, ResetPassword.class);
                startActivity(kyc);

            }
        });




    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
