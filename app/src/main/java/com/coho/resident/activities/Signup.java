package com.coho.resident.activities;

/**
 * Created by Samar on 12/3/2015.
 */

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.coho.resident.R;
import com.coho.resident.Utilities.Constants;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Signup extends AppCompatActivity implements View.OnClickListener {
    EditText name, mobileNum, emailId, password, confirmPassword;
    String getName, getMobileNum, getEmailId, getPassword, getConfirmPassword, mobileError, emailError, passwordError;
    Button signUp;
    ProgressDialog progress;
    Toast mToast;
    private View mSignUpFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        TextView logIn = (TextView) findViewById(R.id.loginTv);
        logIn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent log = new Intent(Signup.this, Login.class);
                startActivity(log);
                finish();
            }
        });
        name = (EditText) findViewById(R.id.firstName);
        mobileNum = (EditText) findViewById(R.id.phoneNum);
        emailId = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        confirmPassword = (EditText) findViewById(R.id.password);
        signUp = (Button) findViewById(R.id.signUpButton);
        signUp.setOnClickListener(this);
        mSignUpFormView = findViewById(R.id.login_form);

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        getParameters();

        if (getName.length() > 0) {
            if (isValidMobNUm(getMobileNum)) {

                if (isValidEmail(getEmailId)) {

                    //if(getPassword.equals(getConfirmPassword)){

                    if (getPassword.length() != 0 && getPassword.length() > 4) {
                        postData(getName, getMobileNum, getEmailId, getPassword, getConfirmPassword);


                    } else {
                        toastMessage("Password should be greater than 4 characters");
                    }

                } else {
                    emailId.setError("Invalid Email");
                    emailId.requestFocus();
                }

            } else {
                mobileNum.setError("Invalid Mobile Number");
                mobileNum.requestFocus();
            }
        } else {
            name.setError("Please enter the name!");
            name.requestFocus();
        }
    }

    public void postData(String getName, String getMobileNum, String getEmailId, String getPassword,
                         String getConfirmPassword) {

        if (Constants.isInternetAvailable(getApplicationContext())) {
            progress = ProgressDialog.show(Signup.this, "Signing up",
                    "Please wait", true);
            Ion.with(getApplicationContext())
                    .load(Constants.COHO_SIGNUP)
                    .setBodyParameter("mobile", getMobileNum)
                    .setBodyParameter("password", getPassword)
                    .setBodyParameter("email", getEmailId)
                    .setBodyParameter("name", getName)
                    .setBodyParameter("c_password", getConfirmPassword)
                    .asString()
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String result) {
                            // do stuff with the result or error
                            Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
                            JSONObject jsonObj = null;
                            try {
                                jsonObj = new JSONObject(result);
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }

                            handleResponse(jsonObj);

                        }
                    });


        }
    }

    public void handleResponse(JSONObject jsonObject) {

        try {
            if (jsonObject.getString("status").equals("success")) {

                Intent hamburgerScreen = new Intent(this, MainActivity.class);
                progress.dismiss();
                startActivity(hamburgerScreen);

            } else {
                if (jsonObject.has("mobile_error")) {
                    mobileError = jsonObject.getString("mobile_error");
                    mobileNum.setError(mobileError);
                    progress.dismiss();
                }
                if (jsonObject.has("email_error")) {
                    emailError = jsonObject.getString("email_error");
                    emailId.setError(emailError);
                    progress.dismiss();
                }
                if (jsonObject.has("pass_error")) {
                    passwordError = jsonObject.getString("pass_error");
                    password.setError(passwordError);
                    confirmPassword.setError(passwordError);
                    progress.dismiss();
                }

                toastMessage(jsonObject.getString("message"));
                progress.dismiss();
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void getParameters() {

        getName = name.getText().toString();
        getMobileNum = mobileNum.getText().toString();
        getEmailId = emailId.getText().toString();
        getPassword = password.getText().toString();
        getConfirmPassword = confirmPassword.getText().toString();

    }

    // validating email id
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    // validating mob num
    private boolean isValidMobNUm(String mobileNum) {

        if (mobileNum.length() == 10)
            return true;
        else
            return false;
    }

    public void toastMessage(String message) {
        if (mToast != null) {
            mToast.cancel();
        }
        mToast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        mToast.show();
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mSignUpFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mSignUpFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mSignUpFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mSignUpFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


}
