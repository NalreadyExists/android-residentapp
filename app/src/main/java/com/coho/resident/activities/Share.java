package com.coho.resident.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.coho.resident.R;

public class Share extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (!(getSupportActionBar() == null)) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }

        Button invite = (Button) findViewById(R.id.invite);


        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        final String cohoid = sharedPreferences.getString("cohoid", "");

        /* "Move into CoHo and enjoy #BetterLiving at #Affordable price! Use my CoHo promo code :" + cohoid +
        " and get 10% off on your rent! Schedule a visit today at www.coho.in");*/

        final String share = "Move to CoHo & enjoy #BetterLiving at affordable price! Use my CoHo code:"
                + cohoid + " & get 10% off on your rent! bit.ly/1UsYXD7";

        invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent msgIntent = new Intent(Intent.ACTION_SEND);
                msgIntent.setType("text/plain");
                msgIntent.putExtra(Intent.EXTRA_TEXT, share);


                startActivity(msgIntent);
            }
        });


       /* FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent msgIntent = new Intent(Intent.ACTION_SEND);
                msgIntent.setType("text/plain");

                startActivity(msgIntent);

                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
