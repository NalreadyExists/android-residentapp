package com.coho.resident.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.coho.resident.R;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

public class PaymentDetail extends AppCompatActivity {

    JSONObject j;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);




        TextView about = (TextView) findViewById(R.id.desc);
        TextView howTo = (TextView) findViewById(R.id.title);
        TextView offer = (TextView) findViewById(R.id.amt);
        TextView location = (TextView) findViewById(R.id.name_vendor);
        Button call =(Button) findViewById(R.id.call);

        String json = getIntent().getStringExtra("json");

        try {
            j= new JSONObject(json);

            ImageView img = (ImageView) findViewById(R.id.logo);
            img.setScaleType(ImageView.ScaleType.FIT_XY);

            Ion.with(img)
                    .error(R.drawable.app_icon)
                    .load(j.getString("image"));

            about.setText(j.getString("title"));
            howTo.setText(j.getString("desc"));
            offer.setText(j.getString("amt"));


        } catch (JSONException e) {
            e.printStackTrace();
        }

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(getApplicationContext(),
                        MainActivityPayment.class);
                try {
                    in.putExtra("amt", j.getString("amt"));
                    in.putExtra("type",j.getString("title"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                startActivity(in);
            }
        });


    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
