package com.coho.resident.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.coho.resident.Domain.Complaint;
import com.coho.resident.R;
import com.coho.resident.Services.JSONParser;
import com.coho.resident.Utilities.Constants;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;


public class Complaints extends AppCompatActivity {
    String[] service = new String[6];
    Spinner spinner;
    String complaintText, complaintType, userId;
    String complaintId;
    JSONObject json;
    ProgressDialog progress;
    JSONArray jsonArray;
    HashMap<String, String> complaintMap = new HashMap<>();
    String url;

    final int REQUEST_IMAGE_CAPTURE = 1, SELECT_FILE = 2;
    ImageView image;
    File destination_temp, destination;
    SharedPreferences sharedPreferences;

    private void selectImage(final int btn) {
        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        if (btn == 1)
                            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    {
                        if (btn == 1)
                            startActivityForResult(
                                    Intent.createChooser(intent, "Select File"),
                                    SELECT_FILE);
                    }
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            try {
                if (extras.get("data") != null) {
                    Bitmap imageBitmap = (Bitmap) extras.get("data");
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    if (imageBitmap != null) {
                        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                    }
                    destination_temp = new File(Environment.getExternalStorageDirectory(),
                            System.currentTimeMillis() + ".jpg");
                    FileOutputStream fo;
                    try {
                        destination_temp.createNewFile();
                        fo = new FileOutputStream(destination_temp);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (requestCode == 1) {
                        image.setImageBitmap(imageBitmap);
                        destination = destination_temp;
                    }
                }


            } catch (Exception e) {


            }
        }

        if (requestCode == SELECT_FILE && resultCode == RESULT_OK) {
            try {
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                CursorLoader cursorLoader = new CursorLoader(this, selectedImageUri, projection, null, null,
                        null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                String selectedImagePath = cursor.getString(column_index);
                Bitmap bm;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(selectedImagePath, options);
                final int REQUIRED_SIZE = 200;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bm = BitmapFactory.decodeFile(selectedImagePath, options);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                destination_temp = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");
                FileOutputStream fo;
                try {
                    destination_temp.createNewFile();
                    fo = new FileOutputStream(destination_temp);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (requestCode == 2) {
                    destination = destination_temp;
                    image.setImageBitmap(bm);
                }
            } catch (Exception e) {

            }
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaints);
        image = (ImageView) findViewById(R.id.image1);


        Button upload = (Button) findViewById(R.id.camera1);

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(1);
            }
        });

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(1);
            }
        });

        ImageButton back = (ImageButton) findViewById(R.id.back_button);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        userId = sharedPreferences.getString("USERID", "");

        try {
            JSONParser parser = new JSONParser();
            json = parser.getJSONData(Constants.COHO_COMPLAINT_TYPE);
            jsonArray = json.getJSONArray("type");
            service = new String[jsonArray.length()];
            for (int i = 0; i < jsonArray.length(); i++) {
                String cName = jsonArray.getJSONObject(i).getString("name");
                String cId = jsonArray.getJSONObject(i).getString("id");
                service[i] = cName;
                complaintMap.put(cName, cId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        final Button btn = (Button) findViewById(R.id.complaint_send_button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress = ProgressDialog.show(Complaints.this, "Sending complaint",
                        "Please wait", true);
                final EditText ct = (EditText) findViewById(R.id.complaint_text);
                btn.setEnabled(false);
                String cText = ct.getText().toString();
                String text = cText.replaceAll(" ", "").replaceAll("\n", "").replaceAll("\r", "");
                if (text.length() == 0) {
                    ct.setError("Enter your complaint first");
                    ct.setText("");
                    btn.setEnabled(true);
                    progress.dismiss();

                } else {

                    complaintText = cText;
//                    complaintText = cText.replaceAll(" ", "%20").replaceAll("\n", "%0A").replaceAll("\r", "%0D");

                    url = Constants.COHO_COMPLAINT_ID +
                            "complaint_id=" + complaintId + "&complain_text=" + complaintText + "&user_id=" + userId;


                    Ion.with(getApplicationContext())
                            .load(Constants.COHO_COMPLAINT_ID_POST_NEW)
                            .setMultipartParameter("complaint_id", complaintId)
                            .setMultipartParameter("complain_text", complaintText)
                            .setMultipartParameter("user_id", userId)
                            .setMultipartFile("complaintimage", "image/jpg", destination)
                            .asString()
                            .setCallback(new FutureCallback<String>() {
                                @Override
                                public void onCompleted(Exception e, String result) {
                                    // do stuff with the result or error
//                            Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
                                    JSONObject jsonObj = null;
                                    try {
                                        jsonObj = new JSONObject(result);
                                        if (jsonObj.getString("status").equals("success")) {
                                            Toast.makeText(Complaints.this, "Complaint successfully registered", Toast.LENGTH_SHORT).show();
                                            ct.setText("");
                                            progress.dismiss();
                                            new CountDownTimer(Toast.LENGTH_SHORT, 1000) {

                                                public void onTick(long millisUntilFinished) {
                                                }

                                                public void onFinish() {
                                                    finish();
                                                    btn.setEnabled(true);

                                                }
                                            }.start();

                                        } else {
                                            Toast.makeText(Complaints.this, "We are not able to process your request right now, please try later.", Toast.LENGTH_SHORT).show();
                                            btn.setEnabled(true);
                                            progress.dismiss();
                                        }
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                        Toast.makeText(Complaints.this,"Sorry we are having some problems right now, please try later",Toast.LENGTH_LONG).show();
                                        progress.dismiss();
                                    }


                                }
                            });


                   /* JSONParser parser = new JSONParser();
                    JSONObject j = parser.getJSONData(url);
                    try {
                        if (j.getString("status").equals("success")) {
                            Toast.makeText(Complaints.this, "Complaint successfully registered", Toast.LENGTH_SHORT).show();
                            ct.setText("");
                            new CountDownTimer(Toast.LENGTH_SHORT, 1000) {

                                public void onTick(long millisUntilFinished) {
                                }

                                public void onFinish() {
                                    finish();

                                }
                            }.start();

                        } else {
                            Toast.makeText(Complaints.this, "We are not able to process your request right now, please try later.", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/
                }


            }
        });

        ArrayAdapter<String> spinnerData = new ArrayAdapter<>(this, R.layout.spinner_back, service);
        spinnerData.setDropDownViewResource
                (R.layout.spinner_item);
        spinner = (Spinner) findViewById(R.id.service_spinner);
        spinner.setAdapter(spinnerData);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                complaintType = spinner.getSelectedItem().toString();
                complaintId = complaintMap.get(complaintType);

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
