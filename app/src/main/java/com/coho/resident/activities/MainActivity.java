package com.coho.resident.activities;

/**
 * Created by Samar on 12/3/2015.
 */

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.coho.resident.R;
import com.coho.resident.Utilities.Constants;
import com.coho.resident.fragment.CoHoPrivilege;
import com.coho.resident.fragment.Concierge;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.koushikdutta.ion.Ion;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.preview.IntercomPreviewPosition;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    String check;
    SharedPreferences sharedPreferences;


    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager viewPager;
    public Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intercom.client().setPreviewPosition(IntercomPreviewPosition.TOP_RIGHT);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        Constants.sendScreenImageName(mTracker, "Main Activity");

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        Boolean isreg = sharedPreferences.getBoolean("isReg", false);
        isreg = false;

        if (!isreg) {
            Intent reg = new Intent(this, Reg.class);
            startActivity(reg);
        }

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        //Fill header as required.
        handleHeader(navigationView);

        navigationView.setNavigationItemSelectedListener(this);

        viewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(viewPager);

        TabLayout tabLayout;
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setTabTextColors(Color.parseColor("#58595b"), Color.parseColor("#58595b"));
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#f46604"));
        tabLayout.setupWithViewPager(viewPager);


        //visibility gone in xml for now and inplemetation moved to concierge assistance
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            //Intercom.client().displayConversationsList();
            }
        });

    }

    private void handleHeader(NavigationView navigationView) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String userId = sharedPreferences.getString("USERID", "");
        String userName = sharedPreferences.getString("USERNAME", "");
        String status = sharedPreferences.getString("status", "");
        String profilePic = sharedPreferences.getString("image", "");
        check = sharedPreferences.getString("COHOSERVICE", "");

        View hView = navigationView.inflateHeaderView(R.layout.nav_header_main);

        TextView uname = (TextView) hView.findViewById(R.id.userName);
        uname.setText(userName);

        TextView cohoId = (TextView) hView.findViewById(R.id.cohoid);
        cohoId.setText(userId);

        TextView txtstatus = (TextView) hView.findViewById(R.id.status);
        if (status.equals("ACTIVE")) {
            txtstatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.green_dot, 0, 0, 0);
            txtstatus.setCompoundDrawablePadding(5);
        }
        txtstatus.setText(status);
        ImageView imageView = (ImageView) hView.findViewById(R.id.profile_pic);

        if (Constants.isInternetAvailable(getApplicationContext())) {
            Ion.with(imageView)
                    .placeholder(R.drawable.app_icon)
                    .error(R.drawable.app_icon)
                    .load(profilePic);
        }


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.log_out) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
            alertDialog.create();
            alertDialog
                    .setMessage("Are you sure you want to log out?");
            alertDialog.setCancelable(false);
            alertDialog.setPositiveButton("Yes",
                    new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {

                            Map userMap = new HashMap();
                            userMap.put("android_app_status", "IN-ACTIVE");
                            Intercom.client().updateUser(userMap);

                            Intercom.client().reset();


                            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.clear();
                            editor.commit();
                            GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(getBaseContext());
                            try {
                                gcm.unregister();
                            } catch (IOException e) {
                                System.out.println("Error Message: " + e.getMessage());
                            }
                            Intent nextScreenTntent = new Intent(MainActivity.this, Login.class);
                            finish();
                            startActivity(nextScreenTntent);
                        }
                    });
            alertDialog.setNegativeButton("No",
                    new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        } else if (id == R.id.coho_card) {

            Intent nextScreenIntent = new Intent(MainActivity.this, CoHoCard.class);
            startActivity(nextScreenIntent);


        } else if (id == R.id.receipts) {

            Intent nextScreenIntent = new Intent(MainActivity.this, Receipts.class);
            startActivity(nextScreenIntent);

        } else if (id == R.id.payments) {

            Intent pay = new Intent(MainActivity.this, Payments.class);
            startActivity(pay);

        } else if (id == R.id.change_password) {

            Intent reset = new Intent(MainActivity.this, ResetPassword.class);
            startActivity(reset);

        } else if (id == R.id.kyc) {

            Intent kyc = new Intent(this, Docs.class);
            startActivity(kyc);

        } else if (id == R.id.rent) {

            Intent kyc = new Intent(this, Share.class);
            startActivity(kyc);

        } else if (id == R.id.feedback) {

            Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_SUBJECT, "Feedback");
            intent.setData(Uri.parse("mailto:resident@coho.in")); // or just "mailto:" for blank
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
            startActivity(intent);

           /* Intent kyc = new Intent(this, Feeedback.class);
            startActivity(kyc);*/

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

      /*  if (check.equals("PG")) {
            adapter.addFragment(new FoodMenu(), "FOOD");
        }
*/

        String type = sharedPreferences.getString("USERTYPE", "");

        adapter.addFragment(new CoHoPrivilege(), "PRIVILEGE");
        adapter.addFragment(new Concierge(), "CONCIERGE");

        /*if (!type.equals("Franchise Owned")) {
            adapter.addFragment(new Complaints(), "CONCIERGE");
        }*/


        viewPager.setAdapter(adapter);
    }

    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}
