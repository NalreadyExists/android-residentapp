package com.coho.resident.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.coho.resident.R;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;


public class CoHoCard extends AppCompatActivity {

    private static Bitmap bmp, thumbnail;
    String userId, userName, status, imageUrl, cohoid;
    JSONObject json;
    JSONArray jsonArray;
    TextView uname, stat, cohoidtxt;
    private FileOutputStream fos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_co_ho_card);

        ImageButton back = (ImageButton) findViewById(R.id.back_button);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        userId = sharedPreferences.getString("USERID", "");
        userName = sharedPreferences.getString("USERNAME", "");
        cohoid = sharedPreferences.getString("cohoid", "");
        status=sharedPreferences.getString("status","");
        imageUrl = sharedPreferences.getString("image","");
        uname = (TextView) findViewById(R.id.user_name);
        stat = (TextView) findViewById(R.id.user_status);
        cohoidtxt = (TextView) findViewById(R.id.cohoid);
        cohoidtxt.setText(cohoid);
        uname.setText(userName);
        final float scale = getApplicationContext().getResources().getDisplayMetrics().density;
        int padding_5dp = (int) (5 * scale + 0.5f);
        if (status.equals("ACTIVE")) {
            stat.setCompoundDrawablesWithIntrinsicBounds(R.drawable.green_dot, 0, 0, 0);
            stat.setCompoundDrawablePadding(padding_5dp);
        }

        ImageView img = (ImageView) findViewById(R.id.user_image);

        Ion.with(img)
                .placeholder(R.drawable.loading)
                .error(R.drawable.app_icon)
                .load(imageUrl);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Contact Us", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });




    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}


