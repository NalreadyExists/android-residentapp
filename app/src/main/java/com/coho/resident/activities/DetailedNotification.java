package com.coho.resident.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.coho.resident.R;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

public class DetailedNotification extends AppCompatActivity {

    JSONObject j;
    TextView heading, desc;
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_notification);
        heading = (TextView) findViewById(R.id.heading);
        desc = (TextView) findViewById(R.id.description);
        image = (ImageView) findViewById(R.id.image);
        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }
        Intent intent = getIntent();

        if (intent.getStringExtra("check").equals("ok")) {
            try {
                j = new JSONObject(intent.getStringExtra("product_object"));

                heading.setText(j.getString("title"));
                desc.setText(j.getString("description"));
                if (j.getString("image").length() > 1) {
                    image.setVisibility(View.VISIBLE);
                    Ion.with(image)
                            .placeholder(R.drawable.loading)
                            .load(j.getString("image"));


                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            heading.setText(intent.getStringExtra("title"));
            desc.setText(intent.getStringExtra("message"));
            if (intent.getStringExtra("url") != null) {
                image.setVisibility(View.VISIBLE);
                Ion.with(image)
                        .placeholder(R.drawable.loading)
                        .load(intent.getStringExtra("url"));
            }
        }

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
