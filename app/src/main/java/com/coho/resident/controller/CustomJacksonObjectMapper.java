package com.coho.resident.controller;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;



public class CustomJacksonObjectMapper extends ObjectMapper {
	private static final long serialVersionUID = 1L;

	public CustomJacksonObjectMapper() {

		setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
		configure(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS, false);
		configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
//		configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
//		setDateFormat(new SimpleDateFormat(DateUtils.FACEBOOK_LONG_DATE_FORMAT)); 
		configure(SerializationFeature.INDENT_OUTPUT, true);
		configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}


}