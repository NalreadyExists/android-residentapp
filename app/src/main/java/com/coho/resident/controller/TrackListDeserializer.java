package com.coho.resident.controller;

import java.io.IOException;
import java.util.List;

import net.theaterears.model.MovieAudioTrack;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class TrackListDeserializer extends JsonDeserializer<List> {

	@Override
	public List<MovieAudioTrack> deserialize(JsonParser jp, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		// TODO Auto-generated method stub
		if (jp.getText().equals("0")) {
			return null;
		} else {
			return (List<MovieAudioTrack>) jp.readValueAs(new TypeReference<List<MovieAudioTrack>>(){});
		}
	}
	

}
