package com.coho.resident.network;


import com.coho.resident.controller.CustomHttpClient;
import com.coho.resident.response.APIResponse;
import com.coho.resident.response.DownloadResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.protocol.HTTP;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;

import newutilities.Logger;

/**
 * This class contains methods for making http request. 
 * @author user
 *
 */

public class RestClient {

	private static final String TAG = RestClient.class.getSimpleName();
	private final static RestClient instance = new RestClient();


	public static final RestClient getInstance() {
		return instance;

	}

	private RestClient() {

	}

	/**
	 * This method is used to make  http get request
	 * @param url: url of the request
	 * @param apiRequest  Api request containing request data
	 * @return resposne of the request.
	 */
	public APIResponse makeHttpGetRequest(String url)	{
		DownloadResponse apiResponse = null;
		HttpGet get = new HttpGet(url);
		get.setHeader("accept", "application/json;charset=utf-8");


		get.setHeader(HTTP.CONTENT_TYPE,"application/json");
		apiResponse = (DownloadResponse)executeRequest(get);
		return apiResponse;
	}

	public APIResponse makeHttpMultiPartRequest(String url,File imageFile)	{
		DownloadResponse apiResponse = null;

		String boundary =  "*****"+Long.toString(System.currentTimeMillis())+"*****";
		MultipartEntityBuilder entity = MultipartEntityBuilder.create();
		entity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
//		entity.setCharset( Charset.forName("UTF-8"));

		entity.setBoundary(boundary);
		entity.addBinaryBody("file", imageFile, ContentType.create("image/jpeg"), imageFile.getName());

		HttpPost put = new HttpPost(url);
		put.setEntity(entity.build());
		put.setHeader("Content-Type","multipart/form-data; boundary="+boundary);

		apiResponse = (DownloadResponse)executeRequest(put);
		return apiResponse;
	}

	public APIResponse makeHttpFormPostRequest(String url,List<NameValuePair> nameValuePairs){

		DownloadResponse apiResponse = new DownloadResponse();
		String response= "";
		HashMap<String,String> respHeaders = new HashMap<String, String>();
		HttpPost httppost = new HttpPost(url);

		httppost.setHeader("accept", "application/json;charset=utf-8");
		httppost.setHeader(HTTP.CONTENT_TYPE,"application/x-www-form-urlencoded;charset=UTF-8");

		HttpClient httpclient = CustomHttpClient.getCustomHttpClient();
		try {
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		// Execute HTTP Post Request
		try {
			HttpResponse httpResponse = httpclient.execute(httppost);
			if (httpResponse != null)	{
				int responseCode = httpResponse.getStatusLine().getStatusCode();
				if(responseCode == 200 || responseCode == 204 ){
					HttpEntity entity = httpResponse.getEntity();

					if (entity != null){
						InputStream instream = entity.getContent();
						response = convertStreamToString(instream);
						instream.close();
						apiResponse.setResponseCode(APIResponse.API_RESPONSE_SUCCESS);
						apiResponse.setdownloadResponse(response);
						apiResponse.setRespHeaders(respHeaders);
					}else{
						apiResponse.setResponseCode(APIResponse.API_RESPONSE_SUCCESS);
						apiResponse.setdownloadResponse("success");
						apiResponse.setRespHeaders(respHeaders);
					}
				}else{
					apiResponse.setResponseCode(APIResponse.API_RESPONSE_FAILURE);
					apiResponse.setErrorCode(responseCode);
					apiResponse.setErrorMessage(response);
					apiResponse.setdownloadResponse(response);
				}
			} else	{
				apiResponse.setResponseCode(APIResponse.API_RESPONSE_FAILURE);
				apiResponse.setErrorCode(0);
				apiResponse.setErrorMessage(response);
				apiResponse.setdownloadResponse(response);
			}

			Logger.log("Response:" , response.toString(), Logger.LogLevel.LOGGING_LEVEL_DEBUG);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return apiResponse;
	}


	/**
	 * This method is used to make HTTP request
	 * @param request: http request
	 * @return: Api resposne
	 */

	private APIResponse executeRequest(HttpUriRequest request)
	{
		DownloadResponse apiResponse = new DownloadResponse();
		HashMap<String,String> respHeaders = new HashMap<String, String>();
		String response= "";
		HttpClient client = CustomHttpClient.getCustomHttpClient();
		HttpResponse httpResponse = null;

		try {

			httpResponse = client.execute(request);
			int responseCode = httpResponse.getStatusLine().getStatusCode();

			if(responseCode == 200 || responseCode == 204 ){
				HttpEntity entity = httpResponse.getEntity();

				if (entity != null){
					InputStream instream = entity.getContent();
					response = convertStreamToString(instream);
					instream.close();
					apiResponse.setResponseCode(APIResponse.API_RESPONSE_SUCCESS);
					apiResponse.setdownloadResponse(response);
					apiResponse.setRespHeaders(respHeaders);
				}else{
					apiResponse.setResponseCode(APIResponse.API_RESPONSE_SUCCESS);
					apiResponse.setdownloadResponse("success");
					apiResponse.setRespHeaders(respHeaders);
				}
			}else{
				apiResponse.setResponseCode(APIResponse.API_RESPONSE_FAILURE);
				apiResponse.setErrorCode(responseCode);
				apiResponse.setErrorMessage(response);
				apiResponse.setdownloadResponse(response);
			}

		} catch (ClientProtocolException e)  {
			client.getConnectionManager().shutdown();
			e.printStackTrace();
			apiResponse.setResponseCode(APIResponse.API_RESPONSE_FAILURE);
			apiResponse.setErrorCode(APIResponse.ERROR_CODE_NO_NETWORK_ERROR);
			apiResponse.setErrorMessage("Connection timeout. Server not responding.");
		} catch (IOException e) {
			client.getConnectionManager().shutdown();
			e.printStackTrace();
			apiResponse.setResponseCode(APIResponse.API_RESPONSE_FAILURE);
			apiResponse.setErrorCode(APIResponse.ERROR_CODE_NO_NETWORK_ERROR);
			apiResponse.setErrorMessage("Connection timeout. Server not responding.");
		}catch (Exception e) {
			apiResponse.setResponseCode(APIResponse.API_RESPONSE_FAILURE);
			apiResponse.setErrorCode(APIResponse.ERROR_CODE_NO_NETWORK_ERROR);
			apiResponse.setErrorMessage("Connection timeout. Server not responding.");
			client.getConnectionManager().shutdown();
			e.printStackTrace();
		}finally {
			if (client != null) {
				if (Logger.ENABLE_LOGGING) {
					Logger.log(TAG, "shutting down connection manager ",
							Logger.LogLevel.LOGGING_LEVEL_DEBUG);
				}
				client.getConnectionManager().shutdown();
				client = null;
			}

		}

		return apiResponse;
	}


	private static String convertStreamToString(InputStream is) {

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

}
