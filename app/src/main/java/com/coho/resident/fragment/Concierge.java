package com.coho.resident.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.coho.resident.R;
import com.coho.resident.Utilities.Constants;
import com.coho.resident.Utilities.NotificationListAdapter;
import com.coho.resident.activities.Complaints2;
import com.coho.resident.activities.DetailedNotification;
import com.coho.resident.activities.Notification;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.annotations.IntercomExclusionStrategy;
import io.intercom.android.sdk.api.IntercomApiInterface;
import io.intercom.android.sdk.preview.IntercomPreviewPosition;
import io.intercom.android.sdk.utilities.IntercomUtils;

/**
 * Created by Samar on 3/2/2016.
 */
public class Concierge extends Fragment {

    ListView notificationList;
    ArrayList<String> heading = new ArrayList<>();
    ArrayList<String> imageAr = new ArrayList<>();
    ArrayList<String> descAr = new ArrayList<>();
    ArrayList<String> dateAr = new ArrayList<>();
    ArrayList<JSONObject> jsonObjects = new ArrayList<>();

    LinearLayout complaints, notifications;

    JSONObject json;
    String url, userId;
    JSONArray jsonArray;

    public Concierge() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.activity_concierge, container, false);

        complaints = (LinearLayout) view.findViewById(R.id.complaints);
        notifications = (LinearLayout) view.findViewById(R.id.notifications);

        //temporily disabled visibility gone in xml file
        notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent noti = new Intent(getActivity(), Notification.class);
                startActivity(noti);
            }
        });


        complaints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intercom.client().displayConversationsList();
                //Intent noti = new Intent(getActivity(), Complaints2.class);
                //startActivity(noti);
            }
        });


        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        userId = sharedPreferences.getString("USERID", "");


        return view;
    }

}


