package com.coho.resident.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.coho.resident.R;
import com.coho.resident.Utilities.Constants;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class FoodMenu extends Fragment {

    String userId;
    private static HashMap<String, String> menuList = new HashMap<>();
    private static HashMap<String, String> menuListLunch = new HashMap<>();
    private static HashMap<String, String> menuListDinner = new HashMap<>();
    View view;
    Button prevBtn;

    String[] service = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
    Spinner spinner;
    String day;
    String complaintId;
    HashMap<String, String> complaintMap = new HashMap<>();


    public FoodMenu() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_food_menu, container, false);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        userId = sharedPreferences.getString("USERID", "");


        if (!(menuList.size() > 0)) {
            if (Constants.isInternetAvailable(getActivity())) {
                Ion.with(this)
                        .load(Constants.COHO_FOOD_MENU + userId)
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                // do stuff with the result or error

                                try {
                                    String res = result.toString();
                                    JSONObject jsonObj;
                                    jsonObj = new JSONObject(res);
                                    JSONArray list = jsonObj.getJSONArray("items");

                                    for (int i = 0; i < list.length(); i++) {
                                        String day = list.getJSONObject(i).getString("day");
                                        String menu = list.getJSONObject(i).getString("menu");
                                        String type = list.getJSONObject(i).getString("type");

                                        if (type.equals("Breakfast")) {
                                            menuList.put(day, menu);
                                        } else if (type.equals("Lunch")) {
                                            menuListLunch.put(day, menu);
                                        } else if (type.equals("Dinner")) {
                                            menuListDinner.put(day, menu);
                                        }
                                    }
                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }

                            }
                        });
            }
        }

        ArrayAdapter<String> spinnerData = new ArrayAdapter<>(getActivity(), R.layout.spinner_back1, service);
        spinnerData.setDropDownViewResource
                (R.layout.spinner_item);
        spinner = (Spinner) view.findViewById(R.id.service_spinner);
        spinner.setAdapter(spinnerData);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                if (menuList.size() == 0) {
                   /* try {
//                        wait(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }*/
                } else {
                    day = spinner.getSelectedItem().toString();

                    try {
                        if (menuList.containsKey(day)) {
                            LinearLayout br = (LinearLayout) view.findViewById(R.id.breakfast_layout);
                            br.setVisibility(View.VISIBLE);
                            TextView brk = (TextView) view.findViewById(R.id.breakfast);
                            brk.setText(menuList.get(day));
                        }
                        if (menuListLunch.containsKey(day)) {
                            LinearLayout br = (LinearLayout) view.findViewById(R.id.lunch_layout);
                            br.setVisibility(View.VISIBLE);
                            TextView lunch = (TextView) view.findViewById(R.id.lunch);
                            lunch.setText(menuListLunch.get(day));
                        } else {
                            LinearLayout br = (LinearLayout) view.findViewById(R.id.lunch_layout);
                            br.setVisibility(View.GONE);
                        }
                        if (menuListDinner.containsKey(day)) {
                            LinearLayout br = (LinearLayout) view.findViewById(R.id.dinner_layout);
                            br.setVisibility(View.VISIBLE);
                            TextView dinner = (TextView) view.findViewById(R.id.dinner);
                            dinner.setText(menuListDinner.get(day));
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

        return view;
    }


}
