package com.coho.resident.fragment;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.coho.resident.R;
import com.coho.resident.Utilities.CohoAdvantageListAdapter;
import com.coho.resident.Utilities.Constants;
import com.coho.resident.activities.DisplayOffersNew;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;


public class CoHoPrivilege extends Fragment {

    ArrayList<String> imageAr = new ArrayList<>();
    ArrayList<String> varAr = new ArrayList<>();


    public CoHoPrivilege() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_co_ho_privilege, container, false);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        String userId = sharedPreferences.getString("USERID", "");
        String userName = sharedPreferences.getString("USERNAME", "");
        String cohoid = sharedPreferences.getString("cohoid", "");
        String status = sharedPreferences.getString("status", "");
        String imageUrl = sharedPreferences.getString("image", "");


        ImageView imageView = (ImageView) view.findViewById(R.id.image_pr);
        TextView cohoName = (TextView) view.findViewById(R.id.name);
        TextView cohoId = (TextView) view.findViewById(R.id.cohoid);
        TextView txtstatus = (TextView) view.findViewById(R.id.status);

        final float scale = getActivity().getResources().getDisplayMetrics().density;
        int padding_5dp = (int) (5 * scale + 0.5f);

        if (status.equals("ACTIVE")) {
            txtstatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.green_dot, 0, 0, 0);
            txtstatus.setCompoundDrawablePadding(padding_5dp);
        }

        txtstatus.setText(status);
        cohoId.setText("CoHo ID : " + cohoid);
        cohoName.setText(userName);

        /*imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.create();
                alertDialog
                        .setMessage("Would you like to update your profile picture");
                alertDialog.setCancelable(false);
                alertDialog.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {

                                Intent KYC = new Intent(getActivity(), com.coho.resident.activities.KYC.class);
                                startActivity(KYC);
                            }
                        });
                alertDialog.setNegativeButton("No",
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
        });*/

        if (Constants.isInternetAvailable(getActivity())) {
            if (!imageUrl.equals("http://www.coho.in/assets/systemimages/userdetails/"))
            Ion.with(imageView)
                    .placeholder(R.drawable.loading)
                    .load(imageUrl);

        }

        imageAr.size();

        if (!(imageAr.size() > 0)) {
            if (Constants.isInternetAvailable(getActivity())) {
                try {

                    Ion.with(getActivity())
                            .load(Constants.COHO_FLYER_NEW)
                            .setBodyParameter("Batman", "romil")
                            .asString()
                            .setCallback(new FutureCallback<String>() {
                                @Override
                                public void onCompleted(Exception e, String result) {
                                    // do stuff with the result or error
                                    JSONObject jsonObj;
                                    try {
                                        jsonObj = new JSONObject(result);
                                        if (jsonObj.getString("status").equals("true")) {
                                            jsonObj = jsonObj.getJSONObject("data");
                                            JSONArray imageJson = jsonObj.getJSONArray("image");
                                            JSONArray varJson = jsonObj.getJSONArray("var");

                                            for (int i = 0; i < imageJson.length(); i++) {
                                                imageAr.add(imageJson.getString(i));
                                                varAr.add(varJson.getString(i));
                                            }

                                            imageAr.size();
                                            varAr.size();
                                        }
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }


                                }
                            }).get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        }


        ListView lv = (ListView) view.findViewById(R.id.categories_listview);
       /* if (imageAr.size() == 0) {

            try {
                wait(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }*/

        CohoAdvantageListAdapter adapter = new CohoAdvantageListAdapter(getActivity(), imageAr, varAr);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                // TODO Auto-generated method stub

                Intent intent = new Intent(getActivity(), DisplayOffersNew.class);
                intent.putExtra("POSITION", position);
                intent.putExtra("var", varAr.get(position));
                startActivity(intent);


            }
        });

        return view;
    }

}
