package com.coho.resident.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.coho.resident.R;
import com.coho.resident.Utilities.Constants;
import com.coho.resident.Utilities.NotificationListAdapter;
import com.coho.resident.activities.DetailedNotification;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

/**
 * Created by Samar on 12/9/2015.
 */
public class Complaints extends Fragment {

    ListView notificationList;
    ArrayList<String> heading = new ArrayList<>();
    ArrayList<String> imageAr = new ArrayList<>();
    ArrayList<String> descAr = new ArrayList<>();
    ArrayList<String> dateAr = new ArrayList<>();
    ArrayList<JSONObject> jsonObjects = new ArrayList<>();

    JSONObject json;
    String url, userId;
    JSONArray jsonArray;

    public Complaints() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_notification, container, false);


        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        userId = sharedPreferences.getString("USERID", "");

        final ListView lv = (ListView) view.findViewById(R.id.notification_list);

        if (imageAr.size() == 0) {

            try {

                Ion.with(getActivity())
                        .load(Constants.COHO_NOTIFICATION)
                        .setBodyParameter("user_id", userId)
                        .asString()
                        .setCallback(new FutureCallback<String>() {
                            @Override
                            public void onCompleted(Exception e, String result) {
                                // do stuff with the result or error
                                JSONObject jsonObj;
                                try {
                                    jsonObj = new JSONObject(result);
                                    if (jsonObj.getString("status").equals("true")) {
                                        JSONArray jsonObj1 = jsonObj.getJSONArray("data");
                                        /*JSONArray imageJson = jsonObj.getJSONArray("image");
                                        JSONArray varJson = jsonObj.getJSONArray("description");
                                        JSONArray headingJson = jsonObj.getJSONArray("title");
*/
                                        for (int i = 0; i < jsonObj1.length(); i++) {
                                            JSONObject j = jsonObj1.getJSONObject(i);
                                            jsonObjects.add(j);
                                            imageAr.add(j.getString("image"));
                                            descAr.add(j.getString("description"));
                                            heading.add(j.getString("title"));
                                            dateAr.add(j.getString("posted_date"));
                                        }

                                        imageAr.size();
                                        descAr.size();
                                    }
                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }


                            }
                        }).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

        }

        NotificationListAdapter adapter = new NotificationListAdapter(getActivity(), imageAr, descAr, heading, dateAr, jsonObjects);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    final int position, long id) {


                JSONObject j = jsonObjects.get(position);

                Intent in = new Intent(getActivity(), DetailedNotification.class);
                in.putExtra("product_object", j.toString());
                in.putExtra("check", "ok");

                startActivity(in);

            }
        });


        Button complaint = (Button) view.findViewById(R.id.logComplaint);

        complaint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent oldCompalints = new Intent(getActivity(), com.coho.resident.activities.Complaints.class);
                startActivity(oldCompalints);
            }
        });

        return view;
    }

}

