package net.theaterears.request;

import android.app.DownloadManager;
import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import net.theaterears.R;
import net.theaterears.controller.CustomJacksonObjectMapper;
import net.theaterears.db.DBStore;
import net.theaterears.model.Advertisement;
import net.theaterears.model.AdvertisementRespose;
import net.theaterears.model.AssistiveParams;
import net.theaterears.model.CheckPaymentResponse;
import net.theaterears.model.CommentAuthor;
import net.theaterears.model.CountryDetailResponse;
import net.theaterears.model.CouponResponse;
import net.theaterears.model.DeviceRegWithGCM;
import net.theaterears.model.ForgetPasswordResponse;
import net.theaterears.model.InitializePaymentResponse;
import net.theaterears.model.LocationResponse;
import net.theaterears.model.LoginDetail;
import net.theaterears.model.MediaUrlAuthResponse;
import net.theaterears.model.MovieComment;
import net.theaterears.model.MovieInDownLoad;
import net.theaterears.model.MoviePost;
import net.theaterears.model.MovieResponse;
import net.theaterears.model.MovieStatusResponse;
import net.theaterears.model.NonceResponse;
import net.theaterears.model.ReviewResponse;
import net.theaterears.model.SignUpResponse;
import net.theaterears.model.State;
import net.theaterears.model.SubscriptionPost;
import net.theaterears.model.SubscriptionResponse;
import net.theaterears.model.TheaterPost;
import net.theaterears.model.TheaterResponse;
import net.theaterears.model.UpdatePaymentResponse;
import net.theaterears.model.UpdateProfileResponse;
import net.theaterears.model.UserProfile;
import net.theaterears.model.UserSubscriptions;
import net.theaterears.network.RestClient;
import net.theaterears.response.APIResponse;
import net.theaterears.response.DownloadResponse;
import net.theaterears.utils.EncryptionHelper;
import net.theaterears.utils.Logger;
import net.theaterears.utils.Logger.LogLevel;
import net.theaterears.utils.ProjectConstants;
import net.theaterears.utils.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

public class RequestProcessor {
    public static final String INDEX_PATH_URL = "http://admin-dev.theaterears.net/wp-content/";

    private final static String TAG = RequestProcessor.class.getSimpleName();
    private static RequestProcessor instance;
    private String srtFileUrl = null;

    private static GoogleCloudMessaging gcmObj;
    private static String regId = "";
    private int updatePaymentFailCount = 0;
    //	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;


    private RequestProcessor() {
    }

    /**
     * method to get instance of class
     *
     * @return : instance of RequestProcessor.
     */
    public static RequestProcessor getInstance() {
        if (instance == null) {
            instance = new RequestProcessor();
        }
        return instance;
    }


    /**
     * This method checks the last updated time of Movies and Theaters
     * data in DB.if time is greater than 24 hours it will again update
     * the Movies and theater data in DB
     *
     * @param context : context of calling activity
     */
    private void checkAndUpdateData(Context context) {
        long lastUpdateTime = Utility.getLongValueFromSharedPrefernce(context, ProjectConstants.UPDATE_TIME_KEY);
        String userName = Utility.getStringValueFromSharedPrefernce(context, ProjectConstants.USER_NAME_KEY);
        if (lastUpdateTime > 0) {
            // check for date change

            long currentTime = System.currentTimeMillis();
            long diff = currentTime - lastUpdateTime;
            long seconds = diff / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;
            if (days >= 1) {
                updateMovieAndTheaterData(context, userName);
            } else {
                return;
            }

        } else {
            updateMovieAndTheaterData(context, userName);
        }
    }

    /**
     * This method first check and update the DB data if
     * required and finally returns the movies list fetched from DB.
     *
     * @param context :  context of calling activity
     * @return :  ArrayList of MoviePost type
     */
    public ArrayList<MoviePost> getMovieList(Context context) {
        checkAndUpdateData(context);
        return DBStore.getInstance(context).getMovieList();
    }

    /**
     * This method first check and update the DB data if required than fetch movies list from DB and finally
     * returns the same by sorting it.
     *
     * @param context  :  context of calling activity
     * @param sortType : sorrting type required
     * @return :  ArrayList of MoviePost type
     */
    public ArrayList<MoviePost> getSortedMovieList(Context context, int sortType, final HashMap<String, Integer> genereIndex) {
        checkAndUpdateData(context);
        ArrayList<MoviePost> movieList = getSortedListFromDB(context, sortType);
        String genereType = "";
        if (movieList != null && movieList.size() > 0) {
            MoviePost post;
            if (sortType == ProjectConstants.SORT_MOVIE_GENRE) {
                for (int i = 0; i < movieList.size(); i++) {
                    post = movieList.get(i);
                    if (post.getCustom_fields() != null && post.getCustom_fields().getGenre() != null) {
                        if (!post.getCustom_fields().getGenre().equalsIgnoreCase(genereType)) {
                            genereType = post.getCustom_fields().getGenre();
                            genereIndex.put(genereType, i);
                            post.setHeaderText(genereType);
                        }
                    }
                }
            }
        }
        return movieList;
    }


    /**
     * register device with gcm
     * @param context context of calling activity
     */
    public static void registerDevice(Context context) {

        if (Utility.getBooleanValueFromSharedPrefernce(context, ProjectConstants.IS_DEVICE_REGISTERED_WITH_GCM)) {
            String regId = Utility.getStringValueFromSharedPrefernce(context, ProjectConstants.DEVICE_REGISTRATION_ID_WITH_GCM);
            if (!(regId != null && regId.length() > 0)) {
                RegisterDeviceWithGCM(context);
            }
        } else {
            RegisterDeviceWithGCM(context);
        }
    }

    /**
     * check if file exists
     * @param URLName url of file
     * @return true if exists otherwise false
     */
    public static boolean fileExists(String URLName) {

        try {
            HttpURLConnection.setFollowRedirects(false);
            // note : you may also need
            //        HttpURLConnection.setInstanceFollowRedirects(false)
            HttpURLConnection con =
                    (HttpURLConnection) new URL(URLName).openConnection();
            con.setRequestMethod("HEAD");
            return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * get sorted lisdt of movie from db
     * @param context context of calling activity
     * @param sortType sort type selected
     * @return sorted movie list
     */
    ArrayList<MoviePost> getSortedListFromDB(Context context, int sortType) {
        switch (sortType) {
            case ProjectConstants.SORT_MOVIE_GENRE:
                return DBStore.getInstance(context).getMovieListSortedByGenere();

            case ProjectConstants.SORT_MOVIE_LATEST:
                return DBStore.getInstance(context).getMovieListSortedByReleaseDate();


            case ProjectConstants.SORT_MOVIE_MPA_RATING:
                return DBStore.getInstance(context).getMovieListSortedByMpaRating();

            case ProjectConstants.SORT_MOVIE_RATING:
                return DBStore.getInstance(context).getMovieListSortedByRating();


            case ProjectConstants.SORT_MOVIE_LANGUAGE:
                return DBStore.getInstance(context).getMovieListSortedByLanguage();


            case ProjectConstants.SORT_MOVIE_ALPHABETICALLY:
                return DBStore.getInstance(context).getMovieListSortedByAlphabetically();


            default:
                return DBStore.getInstance(context).getMovieListSortedByReleaseDate();
        }
    }

    /**
     * get sorted movie from db
     * @param context context of calling activity
     * @param sortType sort type selected
     * @param theaterId theaterid selected
     * @return sorted movie list
     */
    ArrayList<MoviePost> getSortedListFromDB(Context context, int sortType, long theaterId) {
        switch (sortType) {
            case ProjectConstants.SORT_MOVIE_GENRE:
                return DBStore.getInstance(context).getMovieForSelectedTheater(theaterId, "genre ASC");

            case ProjectConstants.SORT_MOVIE_LATEST:
                return DBStore.getInstance(context).getMovieForSelectedTheater(theaterId, "release_date DESC");


            case ProjectConstants.SORT_MOVIE_MPA_RATING:
                return DBStore.getInstance(context).getMovieForSelectedTheater(theaterId, "mpaa_rating ASC");

            case ProjectConstants.SORT_MOVIE_RATING:
                return DBStore.getInstance(context).getMovieForSelectedTheater(theaterId, "CAST(user_rating as REAL) DESC");


            case ProjectConstants.SORT_MOVIE_LANGUAGE:
                return DBStore.getInstance(context).getMovieForSelectedTheater(theaterId, "primary_language ASC");


            case ProjectConstants.SORT_MOVIE_ALPHABETICALLY:
                return DBStore.getInstance(context).getMovieForSelectedTheater(theaterId, "title ASC");


            default:
                return DBStore.getInstance(context).getMovieForSelectedTheater(theaterId, "release_date DESC");
        }
    }

    public ArrayList<MoviePost> getSortedMovieList(Context context, int sortType, final HashMap<String, Integer> genereIndex, long theaterId) {
        checkAndUpdateData(context);
        ArrayList<MoviePost> movieList = new ArrayList<MoviePost>();
        movieList = DBStore.getInstance(context).getMovieForSelectedTheater(theaterId);
        String genereType = "";
        if (movieList != null && movieList.size() > 0) {
            movieList = getSortedListFromDB(context, sortType, theaterId);
            MoviePost post;
            if (sortType == ProjectConstants.SORT_MOVIE_GENRE) {
                for (int i = 0; i < movieList.size(); i++) {
                    post = movieList.get(i);
                    if (post.getCustom_fields() != null && post.getCustom_fields().getGenre() != null) {
                        if (!post.getCustom_fields().getGenre().equalsIgnoreCase(genereType)) {
                            genereType = post.getCustom_fields().getGenre();
                            genereIndex.put(genereType, i);
                            post.setHeaderText(genereType);
                        }
                    }
                }
            }
        }


        return movieList;
    }

    /**
     * This method first check and update the DB data if required than fetch theater list from DB
     * and finally returns the same by sorting it Alphabetically.
     *
     * @param context  :  context of calling activity
     * @param location : sorting type required
     * @return :  ArrayList of TheaterPost type
     */
    public ArrayList<TheaterPost> getTheaterListSortedByAlphabetically(Context context, Location location) {
        checkAndUpdateData(context);
        return DBStore.getInstance(context).getTheaterListSortedAlphabeticaly(location);
    }

    /**
     * This method first check and update the DB data if required than fetch theater list from DB and finally
     * returns the same by sorting it in accordance with its distance from user's currenet location.
     *
     * @param context  :  context of calling activity
     * @param location : sorting type required
     * @return :  ArrayList of TheaterPost type
     */
    public ArrayList<TheaterPost> getTheaterListSortedByLocation(Context context, Location location) {
        checkAndUpdateData(context);
        ArrayList<TheaterPost> theaterPost = DBStore.getInstance(context).getTheaterListSortedAlphabeticaly(location);
        if (location == null) {
            return theaterPost;
        }
        Collections.sort(theaterPost);
        return theaterPost;
    }

    /**
     * delete all the content, downloaded on device for TE movies
     * @param context context of calling activity
     */
    private void deleteDownloadedData(Context context) {
        ArrayList<MoviePost> downloadedMovie = DBStore.getInstance(context).getDownloadedMovieList();
        if (downloadedMovie != null && downloadedMovie.size() > 0) {
            for (MoviePost post : downloadedMovie) {
                File nativeFile = new File(post.getNativePath());
                File indexFile = new File(post.getIndexPath());
                File srtFile = new File(post.getSrtFilePath());
                if (srtFile != null && srtFile.length() > 0) {
                    Utility.deleteAudioFromDevice(srtFile);
                }
                if (nativeFile != null && nativeFile.length() > 0) {
                    Utility.deleteAudioFromDevice(nativeFile);
                }
                if (indexFile != null && indexFile.length() > 0) {
                    Utility.deleteAudioFromDevice(indexFile);
                }
            }
            DBStore.getInstance(context).deleteDownloadedMovies(downloadedMovie);
        }
    }

    /**
     * sign up api request
     * @param context context of calling activity
     * @param loginDetail lg=ogin details of user
     * @return response in SignUpResponse
     */
    public SignUpResponse userSignUpRequest(Context context, LoginDetail loginDetail) {
        registerDevice(context);
        getPreferedLanguage(context);
        downloadSyncMp3(context,"es");
        downloadSyncMp3(context,"en");

        SignUpResponse signUpResponse = new SignUpResponse();

        if (loginDetail == null) {
            signUpResponse.setStatus(ProjectConstants.STATUS_ERROR);
            signUpResponse.setError(context.getResources().getString(R.string.sign_up_details_error_msg));
            return signUpResponse;

        }

        NonceResponse nonceResponse = generateNonce(context);
        if (nonceResponse == null || ProjectConstants.STATUS_ERROR.equalsIgnoreCase(nonceResponse.getStatus())) {
            signUpResponse.setStatus(ProjectConstants.STATUS_ERROR);
            if (nonceResponse == null) {
                signUpResponse.setError(context.getResources().getString(R.string.nonce_error_msg));
            } else {
                signUpResponse.setError(nonceResponse.getError());
            }
            return signUpResponse;
        }

        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("nonce", nonceResponse.getNonce()));
        nameValuePairs.add(new BasicNameValuePair("user_pass", loginDetail.getUser_pass()));
        nameValuePairs.add(new BasicNameValuePair("first_name", loginDetail.getFirst_name()));
        nameValuePairs.add(new BasicNameValuePair("last_name", loginDetail.getLast_name()));
        nameValuePairs.add(new BasicNameValuePair("username", loginDetail.getUsername()));
        nameValuePairs.add(new BasicNameValuePair("email", loginDetail.getEmail()));
        nameValuePairs.add(new BasicNameValuePair("auth_type", loginDetail.getAuth_type()));
        nameValuePairs.add(new BasicNameValuePair("device_id", loginDetail.getDevice_id()));
        nameValuePairs.add(new BasicNameValuePair("latitude", "" + loginDetail.getLatitude()));
        nameValuePairs.add(new BasicNameValuePair("longitude", "" + loginDetail.getLongitude()));
        nameValuePairs.add(new BasicNameValuePair("city", "" + loginDetail.getCity()));
        nameValuePairs.add(new BasicNameValuePair("state", "" + loginDetail.getState()));
        nameValuePairs.add(new BasicNameValuePair("country", "" + loginDetail.getCountry()));
        nameValuePairs.add(new BasicNameValuePair("te_user_mobile", "" + loginDetail.getTe_user_mobile()));

        if (loginDetail.isTermsAgreed()) {
            nameValuePairs.add(new BasicNameValuePair("terms_agreed", "true"));
        } else {
            nameValuePairs.add(new BasicNameValuePair("terms_agreed", "false"));
        }
        if (loginDetail.isPrivacyAgreed()) {
            nameValuePairs.add(new BasicNameValuePair("privacy_policy_agreed", "true"));
        } else {
            nameValuePairs.add(new BasicNameValuePair("privacy_policy_agreed", "false"));
        }
        nameValuePairs.add(new BasicNameValuePair("preferred_language", loginDetail.getPreferred_language()));


        DownloadResponse response = (DownloadResponse) RestClient.getInstance().makeHttpFormPostRequest(ProjectConstants.SERVER_URL + "user/register/", nameValuePairs, loginDetail.getUsername());
        if (response.getResponseCode() == APIResponse.API_RESPONSE_SUCCESS) {
            Logger.log(TAG, "HTTP Request Successful ", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "-- parsing response --", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "Response: " + response.getdownloadResponse(), LogLevel.LOGGING_LEVEL_DEBUG);
            CustomJacksonObjectMapper mapper = new CustomJacksonObjectMapper();
            try {
                signUpResponse = mapper.readValue(response.getdownloadResponse(), SignUpResponse.class);

                if (ProjectConstants.STATUS_OK.equalsIgnoreCase(signUpResponse.getStatus())) {

                    //deleting the downloaded data of previous user
                    deleteDownloadedData(context);

                    //uploading user image on server
                    final String filePath = loginDetail.getFilePath();
                    final String cookie = signUpResponse.getCookie();
                    if (filePath != null) {
                        Thread thread = new Thread() {
                            public void run() {
                                File file = new File(filePath);
                                String uploadCookie = cookie.replace("|", "%7C");
                                RestClient.getInstance().makeHttpMultiPartRequest(ProjectConstants.SERVER_URL + "user/upload_spy_image_capture?cookie=" + uploadCookie, file, "");

                            }
                        };
                        thread.start();
                    }
                    loginDetail.setId(signUpResponse.getUser_id());
                    DBStore.getInstance(context).addUserDataInDB(loginDetail);
                    getAdvertisement(context, loginDetail.getUsername());
                    try {
                        Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.SIGN_IN_TYPE_KEY, EncryptionHelper.encrypt(ProjectConstants.ENCRYPTION_KEY, loginDetail.getAuth_type()));
                        Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.USER_NAME_KEY, EncryptionHelper.encrypt(ProjectConstants.ENCRYPTION_KEY, loginDetail.getUsername()));
                        Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.PASSWORD_KEY, EncryptionHelper.encrypt(ProjectConstants.ENCRYPTION_KEY, loginDetail.getUser_pass()));
                        Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.COOKIE_KEY, EncryptionHelper.encrypt(ProjectConstants.ENCRYPTION_KEY, signUpResponse.getCookie()));
                    } catch (Exception e) {

                        Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.SIGN_IN_TYPE_KEY, loginDetail.getAuth_type());
                        Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.USER_NAME_KEY, loginDetail.getUsername());
                        Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.PASSWORD_KEY, loginDetail.getUser_pass());
                        Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.COOKIE_KEY, signUpResponse.getCookie());
                    }
                } else {
                    return signUpResponse;
                }

            } catch (JsonParseException e) {
                signUpResponse.setStatus(ProjectConstants.STATUS_ERROR);
                signUpResponse.setError(context.getResources().getString(R.string.parsing_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return signUpResponse;
            } catch (JsonMappingException e) {
                signUpResponse.setStatus(ProjectConstants.STATUS_ERROR);
                signUpResponse.setError(context.getResources().getString(R.string.parsing_mapping_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return signUpResponse;
            } catch (IOException e) {
                signUpResponse.setStatus(ProjectConstants.STATUS_ERROR);
                signUpResponse.setError(context.getResources().getString(R.string.parsing_IO_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return signUpResponse;
            }
        } else {
            signUpResponse.setStatus(ProjectConstants.STATUS_ERROR);
            signUpResponse.setError(context.getResources().getString(R.string.http_error_msg));
            return signUpResponse;
        }

        return signUpResponse;

    }

    /**
     * api to get the advertisemt
     * @param context context of calling activity
     * @param userName te user name
     * @return response in AdvertisementRespose
     */
    private AdvertisementRespose getAdvertisement(Context context, String userName) {

        StringBuilder stringBuilder = new StringBuilder(ProjectConstants.SERVER_URL);
        stringBuilder.append(ProjectConstants.ADVERTISEMENT_URL);


        AdvertisementRespose adResponse = new AdvertisementRespose();
        DownloadResponse response = (DownloadResponse) RestClient.getInstance().makeHttpGetRequest(stringBuilder.toString(), userName);
        if (response.getResponseCode() == APIResponse.API_RESPONSE_SUCCESS) {
            Logger.log(TAG, "HTTP Request Successful ", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "-- parsing response --", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "Response: " + response.getdownloadResponse(), LogLevel.LOGGING_LEVEL_DEBUG);
            CustomJacksonObjectMapper mapper = new CustomJacksonObjectMapper();
            try {
                adResponse = mapper.readValue(response.getdownloadResponse(), AdvertisementRespose.class);
            } catch (JsonParseException e) {
                adResponse.setStatus(ProjectConstants.STATUS_ERROR);
                adResponse.setError(context.getResources().getString(R.string.parsing_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return adResponse;
            } catch (JsonMappingException e) {
                adResponse.setStatus(ProjectConstants.STATUS_ERROR);
                adResponse.setError(context.getResources().getString(R.string.parsing_mapping_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return adResponse;
            } catch (IOException e) {
                adResponse.setStatus(ProjectConstants.STATUS_ERROR);
                adResponse.setError(context.getResources().getString(R.string.parsing_IO_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return adResponse;
            }
        } else {
            adResponse.setStatus(ProjectConstants.STATUS_ERROR);
            adResponse.setError(context.getResources().getString(R.string.http_error_msg));
            return adResponse;
        }

        if (adResponse != null && ProjectConstants.STATUS_OK.equalsIgnoreCase(adResponse.getStatus())) {
            DBStore.getInstance(context).clearAdsTable();
            ArrayList<Advertisement> moviePosts = adResponse.getPosts();
            if (moviePosts != null && moviePosts.size() > 0) {
                boolean result = DBStore.getInstance(context).addAdvertisementInDB(moviePosts);
                if (!result) {
                    adResponse.setStatus(ProjectConstants.STATUS_ERROR);
                    adResponse.setError(context.getResources().getString(R.string.database_error_msg));
                }
            }
        }
        return adResponse;
    }

    /**
     * update subscription request api
     * @param context context of calling activity
     * @param userName TE user name
     * @return response in SubscriptionResponse
     */
    private SubscriptionResponse updateSubscription(Context context, String userName) {


        StringBuilder stringBuilder = new StringBuilder(ProjectConstants.SERVER_URL);
        stringBuilder.append(ProjectConstants.SUBSCRIPTION_URL);

        SubscriptionResponse subscriptionResponse = new SubscriptionResponse();
        DownloadResponse response = (DownloadResponse) RestClient.getInstance().makeHttpGetRequest(stringBuilder.toString(), userName);
        if (response.getResponseCode() == APIResponse.API_RESPONSE_SUCCESS) {
            Logger.log(TAG, "HTTP Request Successful ", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "-- parsing response --", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "Response: " + response.getdownloadResponse(), LogLevel.LOGGING_LEVEL_DEBUG);
            CustomJacksonObjectMapper mapper = new CustomJacksonObjectMapper();
            try {
                subscriptionResponse = mapper.readValue(response.getdownloadResponse(), SubscriptionResponse.class);
            } catch (JsonParseException e) {
                subscriptionResponse.setStatus(ProjectConstants.STATUS_ERROR);
                subscriptionResponse.setError(context.getResources().getString(R.string.parsing_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return subscriptionResponse;
            } catch (JsonMappingException e) {
                subscriptionResponse.setStatus(ProjectConstants.STATUS_ERROR);
                subscriptionResponse.setError(context.getResources().getString(R.string.parsing_mapping_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return subscriptionResponse;
            } catch (IOException e) {
                subscriptionResponse.setStatus(ProjectConstants.STATUS_ERROR);
                subscriptionResponse.setError(context.getResources().getString(R.string.parsing_IO_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return subscriptionResponse;
            }
        } else {
            subscriptionResponse.setStatus(ProjectConstants.STATUS_ERROR);
            subscriptionResponse.setError(context.getResources().getString(R.string.http_error_msg));
            return subscriptionResponse;
        }

        if (subscriptionResponse != null && ProjectConstants.STATUS_OK.equalsIgnoreCase(subscriptionResponse.getStatus())) {
            DBStore.getInstance(context).clearSubscriptionTable();
            ArrayList<SubscriptionPost> subscriptionPosts = subscriptionResponse.getPosts();
            if (subscriptionPosts != null && subscriptionPosts.size() > 0) {
                boolean result = DBStore.getInstance(context).addSubscriptionInDB(subscriptionPosts, userName);
                if (!result) {
                    subscriptionResponse.setStatus(ProjectConstants.STATUS_ERROR);
                    subscriptionResponse.setError(context.getResources().getString(R.string.database_error_msg));
                }
            }
        }
        return subscriptionResponse;
    }

    /**
     * update movie request api
     * @param context context of calling activity
     * @param posts theaters for whcih movies has to updated
     * @param userName TE user name
     * @return response in MovieResponse
     */
    private MovieResponse updateMovie(Context context, ArrayList<TheaterPost> posts, String userName) {


        StringBuilder stringBuilder = new StringBuilder(ProjectConstants.SERVER_URL);
        stringBuilder.append(ProjectConstants.MOVIE_URL);
        String pipe = "";
        for (TheaterPost post : posts) {
            stringBuilder.append(pipe);
            stringBuilder.append("x");
            stringBuilder.append(post.getId());
            stringBuilder.append("x");
            pipe = "%7C";
        }

        MovieResponse movieResponse = new MovieResponse();
        DownloadResponse response = (DownloadResponse) RestClient.getInstance().makeHttpGetRequest(stringBuilder.toString(), userName);
        if (response.getResponseCode() == APIResponse.API_RESPONSE_SUCCESS) {
            Logger.log(TAG, "HTTP Request Successful ", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "-- parsing response --", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "Response: " + response.getdownloadResponse(), LogLevel.LOGGING_LEVEL_DEBUG);
            CustomJacksonObjectMapper mapper = new CustomJacksonObjectMapper();
            try {
                movieResponse = mapper.readValue(response.getdownloadResponse(), MovieResponse.class);
            } catch (JsonParseException e) {
                movieResponse.setStatus(ProjectConstants.STATUS_ERROR);
                movieResponse.setError(context.getResources().getString(R.string.parsing_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return movieResponse;
            } catch (JsonMappingException e) {
                movieResponse.setStatus(ProjectConstants.STATUS_ERROR);
                movieResponse.setError(context.getResources().getString(R.string.parsing_mapping_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return movieResponse;
            } catch (IOException e) {
                movieResponse.setStatus(ProjectConstants.STATUS_ERROR);
                movieResponse.setError(context.getResources().getString(R.string.parsing_IO_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return movieResponse;
            }
        } else {
            movieResponse.setStatus(ProjectConstants.STATUS_ERROR);
            movieResponse.setError(context.getResources().getString(R.string.http_error_msg));
            return movieResponse;
        }

        if (movieResponse != null && ProjectConstants.STATUS_OK.equalsIgnoreCase(movieResponse.getStatus())) {
            DBStore.getInstance(context).clearMovieDataTable();
            ArrayList<MoviePost> moviePosts = movieResponse.getPosts();
            if (moviePosts != null && moviePosts.size() > 0) {
                updateSubscription(context,userName);
                boolean result = DBStore.getInstance(context).addMovieInDB(moviePosts, userName);
                if (!result) {
                    movieResponse.setStatus(ProjectConstants.STATUS_ERROR);
                    movieResponse.setError(context.getResources().getString(R.string.database_error_msg));
                }
            }
        }
        return movieResponse;
    }


    /**
     * update state city request api
     * @param context context of calling activity
     * @param userName TE user name
     * @return true if updated otherwise false
     */
    private boolean updateStateCity(Context context, String userName) {

        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("country", "United States"));
        if (ProjectConstants.PRODUCTION) {
            nameValuePairs.add(new BasicNameValuePair("theater", "production"));
        }


        CountryDetailResponse countryDetailResponse = new CountryDetailResponse();
        DownloadResponse response = (DownloadResponse) RestClient.getInstance().makeHttpFormPostRequest(ProjectConstants.SERVER_URL + "options/get_states/", nameValuePairs, userName);
        if (response.getResponseCode() == APIResponse.API_RESPONSE_SUCCESS) {
            Logger.log(TAG, "HTTP Request Successful ", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "-- parsing response --", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "Response: " + response.getdownloadResponse(), LogLevel.LOGGING_LEVEL_DEBUG);
            CustomJacksonObjectMapper mapper = new CustomJacksonObjectMapper();
            try {
                countryDetailResponse = mapper.readValue(response.getdownloadResponse(), CountryDetailResponse.class);
            } catch (JsonParseException e) {
                countryDetailResponse.setStatus(ProjectConstants.STATUS_ERROR);
                countryDetailResponse.setError(context.getResources().getString(R.string.parsing_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return false;
            } catch (JsonMappingException e) {
                countryDetailResponse.setStatus(ProjectConstants.STATUS_ERROR);
                countryDetailResponse.setError(context.getResources().getString(R.string.parsing_mapping_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return false;
            } catch (IOException e) {
                countryDetailResponse.setStatus(ProjectConstants.STATUS_ERROR);
                countryDetailResponse.setError(context.getResources().getString(R.string.parsing_IO_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return false;
            }
        } else {
            return false;
        }

        if (countryDetailResponse != null && ProjectConstants.STATUS_OK.equalsIgnoreCase(countryDetailResponse.getStatus())) {
            DBStore.getInstance(context).clearStateCityTable();
            ArrayList<State> states = countryDetailResponse.getStates();
            if (states != null && states.size() > 0) {
                return DBStore.getInstance(context).addStateCityInDB(states);
            }
        }
        return false;
    }

    /**
     * update theater request api
     * @param context context of calling activity
     * @param location location to get the theaterears
     * @param userName TE user name
     * @return response in TheaterResponse
     */
    private TheaterResponse updateTheaters(Context context, String location, String userName) {

        updateStateCity(context, userName);

        if (!DBStore.getInstance(context).isLanguageExists()) {
            getAllLanguage(context);
        }

        Utility.saveLongValueInSharedPrefrence(context, ProjectConstants.UPDATE_TIME_KEY, System.currentTimeMillis());
        StringBuilder requestBuilder = new StringBuilder(ProjectConstants.SERVER_URL);
        requestBuilder.append(ProjectConstants.THEATER_URL);
        requestBuilder.append(location.replace(" ", "%20"));

        TheaterResponse theaterResponse = new TheaterResponse();
        DownloadResponse response = (DownloadResponse) RestClient.getInstance().makeHttpGetRequest(requestBuilder.toString(), userName);

        if (response.getResponseCode() == APIResponse.API_RESPONSE_SUCCESS) {
            Logger.log(TAG, "HTTP Request Successful ", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "-- parsing response --", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "Response: " + response.getdownloadResponse(), LogLevel.LOGGING_LEVEL_DEBUG);
            CustomJacksonObjectMapper mapper = new CustomJacksonObjectMapper();
            try {
                theaterResponse = mapper.readValue(response.getdownloadResponse(), TheaterResponse.class);
            } catch (JsonParseException e) {
                theaterResponse.setStatus(ProjectConstants.STATUS_ERROR);
                theaterResponse.setError(context.getResources().getString(R.string.parsing_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return theaterResponse;
            } catch (JsonMappingException e) {
                theaterResponse.setStatus(ProjectConstants.STATUS_ERROR);
                theaterResponse.setError(context.getResources().getString(R.string.parsing_mapping_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return theaterResponse;
            } catch (IOException e) {
                theaterResponse.setStatus(ProjectConstants.STATUS_ERROR);
                theaterResponse.setError(context.getResources().getString(R.string.parsing_IO_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return theaterResponse;
            }
        } else {
            theaterResponse.setStatus(ProjectConstants.STATUS_ERROR);
            theaterResponse.setError(context.getResources().getString(R.string.http_error_msg));
            return theaterResponse;
        }

        if (theaterResponse != null && ProjectConstants.STATUS_OK.equalsIgnoreCase(theaterResponse.getStatus())) {
            DBStore.getInstance(context).clearTheaterTable();
            ArrayList<TheaterPost> theaterPosts = theaterResponse.getPosts();
            if (theaterPosts != null && theaterPosts.size() > 0) {
                boolean result = DBStore.getInstance(context).addTheatersInDB(theaterPosts);
                if (!result) {
                    theaterResponse.setStatus(ProjectConstants.STATUS_ERROR);
                    theaterResponse.setError(context.getResources().getString(R.string.database_error_msg));
                }
            }
        }
        return theaterResponse;

    }

    /**
     * get address string from the  state, city , country present in shared prefences
     * @param context context of calling activity
     * @return
     */
    private String getAddressFromLocation(Context context) {
        StringBuilder addressString = new StringBuilder();
        // Locality is usually a city
        String city = Utility.getStringValueFromSharedPrefernce(context, ProjectConstants.LOCATION_SELECTION_CITY_KEY);
        if (city == null || city.length() <= 0) {
            return null;
        }
        addressString.append(city);
        addressString.append("%7C");
        String state = Utility.getStringValueFromSharedPrefernce(context, ProjectConstants.LOCATION_SELECTION_STATE_KEY);
        if (state == null || state.length() <= 0) {
            return null;
        }
        addressString.append(state);
        addressString.append("%7C");
        String country = Utility.getStringValueFromSharedPrefernce(context, ProjectConstants.LOCATION_SELECTION_COUNTRY_KEY);
        if (country == null || country.length() <= 0) {
            return null;
        }
        addressString.append(country);
        return addressString.toString();
    }

    /**
     * request api to genereate the nonce
     * @param context context of calling activity
     * @return response in NonceResponse
     */
    public NonceResponse generateNonce(Context context) {
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("controller", "user"));
        nameValuePairs.add(new BasicNameValuePair("method", "register"));

        NonceResponse nonceResponse = new NonceResponse();
        DownloadResponse response = (DownloadResponse) RestClient.getInstance().makeHttpFormPostRequest(ProjectConstants.SERVER_URL + "get_nonce/", nameValuePairs, "");
        if (response.getResponseCode() == APIResponse.API_RESPONSE_SUCCESS) {
            Logger.log(TAG, "HTTP Request Successful ", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "-- parsing response --", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "Response: " + response.getdownloadResponse(), LogLevel.LOGGING_LEVEL_DEBUG);
            CustomJacksonObjectMapper mapper = new CustomJacksonObjectMapper();
            try {
                nonceResponse = mapper.readValue(response.getdownloadResponse(), NonceResponse.class);
            } catch (JsonParseException e) {
                nonceResponse.setStatus(ProjectConstants.STATUS_ERROR);
                nonceResponse.setError(context.getResources().getString(R.string.parsing_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
            } catch (JsonMappingException e) {
                nonceResponse.setStatus(ProjectConstants.STATUS_ERROR);
                nonceResponse.setError(context.getResources().getString(R.string.parsing_mapping_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
            } catch (IOException e) {
                nonceResponse.setStatus(ProjectConstants.STATUS_ERROR);
                nonceResponse.setError(context.getResources().getString(R.string.parsing_IO_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
            }
        } else {
            nonceResponse.setStatus(ProjectConstants.STATUS_ERROR);
            nonceResponse.setError(context.getResources().getString(R.string.http_error_msg));
        }
        return nonceResponse;
    }

    /**
     * forgot password api rewuest
     * @param context context of calling activity
     * @param emailId email id of user
     * @return response in ForgetPasswordResponse
     */
    public ForgetPasswordResponse sendForgetPasswordRequest(Context context, String emailId) {

        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("user_login", emailId));

        nameValuePairs.add(new BasicNameValuePair("user_login", emailId));

        ForgetPasswordResponse forgetPassResponse = new ForgetPasswordResponse();
        DownloadResponse response = (DownloadResponse) RestClient.getInstance().makeHttpFormPostRequest(ProjectConstants.SERVER_URL + "user/retrieve_password/", nameValuePairs, emailId);
        if (response.getResponseCode() == APIResponse.API_RESPONSE_SUCCESS) {
            Logger.log(TAG, "HTTP Request Successful ", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "-- parsing response --", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "Response: " + response.getdownloadResponse(), LogLevel.LOGGING_LEVEL_DEBUG);
            CustomJacksonObjectMapper mapper = new CustomJacksonObjectMapper();
            try {
                forgetPassResponse = mapper.readValue(response.getdownloadResponse(), ForgetPasswordResponse.class);
            } catch (JsonParseException e) {
                forgetPassResponse.setStatus(ProjectConstants.STATUS_ERROR);
                forgetPassResponse.setError(context.getResources().getString(R.string.parsing_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
            } catch (JsonMappingException e) {
                forgetPassResponse.setStatus(ProjectConstants.STATUS_ERROR);
                forgetPassResponse.setError(context.getResources().getString(R.string.parsing_mapping_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
            } catch (IOException e) {
                forgetPassResponse.setStatus(ProjectConstants.STATUS_ERROR);
                forgetPassResponse.setError(context.getResources().getString(R.string.parsing_IO_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
            }
        } else {
            forgetPassResponse.setStatus(ProjectConstants.STATUS_ERROR);
            forgetPassResponse.setError(context.getResources().getString(R.string.http_error_msg));
        }
        return forgetPassResponse;
    }


    /**
     * add rate and review request api
     * @param context context of calling activity
     * @param movieId id of movie for which review are added
     * @param comment comments to update
     * @param isMovieReviewed is movie already reviewed
     * @return response in ReviewResponse
     */
    public ReviewResponse addRateAndReview(Context context, long movieId, MovieComment comment, boolean isMovieReviewed) {

        ReviewResponse reviewResponse = new ReviewResponse();
        String cookie = Utility.getStringValueFromSharedPrefernce(context, ProjectConstants.COOKIE_KEY);

        if (cookie != null && cookie.length() > 0) {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair("cookie", cookie));
            nameValuePairs.add(new BasicNameValuePair("post_id", "" + movieId));
            nameValuePairs.add(new BasicNameValuePair("content", comment.getContent()));
            nameValuePairs.add(new BasicNameValuePair("rating", comment.getRating()));


            DownloadResponse response = (DownloadResponse) RestClient.getInstance().makeHttpFormPostRequest(ProjectConstants.SERVER_URL + "user/post_comment/", nameValuePairs, comment.getName());
            if (response.getResponseCode() == APIResponse.API_RESPONSE_SUCCESS) {
                Logger.log(TAG, "HTTP Request Successful ", LogLevel.LOGGING_LEVEL_DEBUG);
                Logger.log(TAG, "-- parsing response --", LogLevel.LOGGING_LEVEL_DEBUG);
                Logger.log(TAG, "Response: " + response.getdownloadResponse(), LogLevel.LOGGING_LEVEL_DEBUG);
                CustomJacksonObjectMapper mapper = new CustomJacksonObjectMapper();
                try {
                    reviewResponse = mapper.readValue(response.getdownloadResponse(), ReviewResponse.class);
                    comment.setComment_id(reviewResponse.getComment_id());
                    LoginDetail detail = DBStore.getInstance(context).getUserLogInDetail();
                    CommentAuthor author = new CommentAuthor();
                    author.setFirst_name(detail.getFirst_name());
                    author.setLast_name(detail.getLast_name());
                    comment.setAuthor(author);
                    DBStore.getInstance(context).addUserMovieCommentsInDB(movieId, comment, isMovieReviewed);
                } catch (JsonParseException e) {
                    reviewResponse.setStatus(ProjectConstants.STATUS_ERROR);
                    reviewResponse.setError(context.getResources().getString(R.string.parsing_fail_error));
                    Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                    return reviewResponse;
                } catch (JsonMappingException e) {
                    reviewResponse.setStatus(ProjectConstants.STATUS_ERROR);
                    reviewResponse.setError(context.getResources().getString(R.string.parsing_mapping_fail_error));
                    Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                    return reviewResponse;
                } catch (IOException e) {
                    reviewResponse.setStatus(ProjectConstants.STATUS_ERROR);
                    reviewResponse.setError(context.getResources().getString(R.string.parsing_IO_fail_error));
                    Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                    return reviewResponse;
                }
            }
            return reviewResponse;
        } else {
            reviewResponse.setStatus(ProjectConstants.STATUS_ERROR);
            reviewResponse.setError(context.getResources().getString(R.string.invalid_cookie_error));
            return reviewResponse;
        }
    }


    /**
     * update profile api request
     * @param context context of calling activity
     * @param params assitive parameters of user
     * @param userName TE user name
     * @return response in UpdateProfileResponse
     */
    public UpdateProfileResponse updateProfile(Context context, AssistiveParams params, String userName) {

        String cookie = Utility.getStringValueFromSharedPrefernce(context, ProjectConstants.COOKIE_KEY);
        UpdateProfileResponse profileResponse = new UpdateProfileResponse();
        if (cookie != null && cookie.length() > 0) {

            StringBuilder requestURL = new StringBuilder(ProjectConstants.SERVER_BASIC_URL + "api/user/update_user_profile/");
            try {
                requestURL.append("?cookie=" + URLEncoder.encode(cookie, "UTF-8"));
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }
            requestURL.append("&f_375=" + params.getF_375().replace(" ", "%20"));
            requestURL.append("&f_750=" + params.getF_375().replace(" ", "%20"));
            requestURL.append("&f_1250=" + params.getF_375().replace(" ", "%20"));
            requestURL.append("&f_2250=" + params.getF_375().replace(" ", "%20"));
            requestURL.append("&f_4500=" + params.getF_375().replace(" ", "%20"));


            DownloadResponse response = (DownloadResponse) RestClient.getInstance().makeHttpGetRequest(requestURL.toString(), userName);

            if (response.getResponseCode() == APIResponse.API_RESPONSE_SUCCESS) {
                Logger.log(TAG, "HTTP Request Successful ", LogLevel.LOGGING_LEVEL_DEBUG);
                Logger.log(TAG, "-- parsing response --", LogLevel.LOGGING_LEVEL_DEBUG);
                Logger.log(TAG, "Response: " + response.getdownloadResponse(), LogLevel.LOGGING_LEVEL_DEBUG);
                CustomJacksonObjectMapper mapper = new CustomJacksonObjectMapper();
                try {
                    profileResponse = mapper.readValue(response.getdownloadResponse(), UpdateProfileResponse.class);
                } catch (JsonParseException e) {
                    profileResponse.setStatus(ProjectConstants.STATUS_ERROR);
                    profileResponse.setError(context.getResources().getString(R.string.parsing_fail_error));
                    Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                    return profileResponse;
                } catch (JsonMappingException e) {
                    profileResponse.setStatus(ProjectConstants.STATUS_ERROR);
                    profileResponse.setError(context.getResources().getString(R.string.parsing_mapping_fail_error));
                    Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                    return profileResponse;
                } catch (IOException e) {
                    profileResponse.setStatus(ProjectConstants.STATUS_ERROR);
                    profileResponse.setError(context.getResources().getString(R.string.parsing_IO_fail_error));
                    Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                    return profileResponse;
                }
            }
            return profileResponse;
        } else {
            profileResponse.setStatus(ProjectConstants.STATUS_ERROR);
            profileResponse.setError(context.getResources().getString(R.string.invalid_cookie_error));
            return profileResponse;
        }
    }


    /**
     * update profile api request
     * @param context context of calling activity
     * @param userName TE user name
     * @return response in UpdateProfileResponse
     */
    public UpdateProfileResponse updateProfile(Context context, UserProfile userProfile, String userName) {
        String cookie = Utility.getStringValueFromSharedPrefernce(context, ProjectConstants.COOKIE_KEY);
        UpdateProfileResponse profileResponse = new UpdateProfileResponse();
        if (cookie != null && cookie.length() > 0) {

            StringBuilder requestURL = new StringBuilder(ProjectConstants.SERVER_BASIC_URL + "api/user/update_user_profile/");
            try {
                requestURL.append("?cookie=" + URLEncoder.encode(cookie, "UTF-8"));
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }
            requestURL.append("&first_name=" + userProfile.getFirst_name().replace(" ", "%20"));
            requestURL.append("&last_name=" + userProfile.getLast_name().replace(" ", "%20"));
            requestURL.append("&preferred_language=" + userProfile.getPreferred_language().replace(" ", "%20"));
            requestURL.append("&country=" + userProfile.getCountry().replace(" ", "%20"));
            requestURL.append("&state=" + userProfile.getState().replace(" ", "%20"));
            requestURL.append("&city=" + userProfile.getCity().replace(" ", "%20"));
            requestURL.append("&te_user_mobile=" + userProfile.getTe_user_mobile().replace(" ", "%20"));

            DownloadResponse response = (DownloadResponse) RestClient
                    .getInstance().makeHttpGetRequest(requestURL.toString(), userName);

            if (response.getResponseCode() == APIResponse.API_RESPONSE_SUCCESS) {
                Logger.log(TAG, "HTTP Request Successful ", LogLevel.LOGGING_LEVEL_DEBUG);
                Logger.log(TAG, "-- parsing response --", LogLevel.LOGGING_LEVEL_DEBUG);
                Logger.log(TAG, "Response: " + response.getdownloadResponse(), LogLevel.LOGGING_LEVEL_DEBUG);
                CustomJacksonObjectMapper mapper = new CustomJacksonObjectMapper();
                try {
                    profileResponse = mapper.readValue(response.getdownloadResponse(), UpdateProfileResponse.class);
                    updateUserSubscription(context, profileResponse.getUser_subscriptions());
                } catch (JsonParseException e) {
                    profileResponse.setStatus(ProjectConstants.STATUS_ERROR);
                    profileResponse.setError(context.getResources().getString(R.string.parsing_fail_error));
                    Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                    return profileResponse;
                } catch (JsonMappingException e) {
                    profileResponse.setStatus(ProjectConstants.STATUS_ERROR);
                    profileResponse.setError(context.getResources().getString(R.string.parsing_mapping_fail_error));
                    Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                    return profileResponse;
                } catch (IOException e) {
                    profileResponse.setStatus(ProjectConstants.STATUS_ERROR);
                    profileResponse.setError(context.getResources().getString(R.string.parsing_IO_fail_error));
                    Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                    return profileResponse;
                }
            }
            return profileResponse;
        } else {
            profileResponse.setStatus(ProjectConstants.STATUS_ERROR);
            profileResponse.setError(context.getResources().getString(R.string.invalid_cookie_error));
            return profileResponse;
        }
    }


    /**
     * get prefrred language api request
     * @param context context of calling activity
     * @return languages in properties file
     */
    public Properties getPreferedLanguage(Context context) {
        Properties properties = new Properties();
        try {
            DownloadResponse response = (DownloadResponse) RestClient.getInstance().makeHttpGetRequest(ProjectConstants.SERVER_BASIC_URL + "preferred-languages.properties", "");
            if (response != null && response.getResponseCode() == APIResponse.API_RESPONSE_SUCCESS) {
                properties.load(new StringReader(response.getdownloadResponse()));
            } else {
                return properties;
            }
        } catch (MalformedURLException e) {
            return properties;
        } catch (IOException e) {
            return properties;
        }
        DBStore.getInstance(context).addLanguageInDB(properties);
        updateStateCity(context, "");
        return properties;
    }

    /**
     * get all languages request api
     * @param context context of calling activity
     */
    public void getAllLanguage(Context context) {
        Properties properties = new Properties();
        try {
            DownloadResponse response = (DownloadResponse) RestClient.getInstance().makeHttpGetRequest(ProjectConstants.SERVER_BASIC_URL + "languages.properties", "");
            if (response != null && response.getResponseCode() == APIResponse.API_RESPONSE_SUCCESS) {
                properties.load(new StringReader(response.getdownloadResponse()));
            } else {
                return;
            }
        } catch (MalformedURLException e) {
            return;
        } catch (IOException e) {
            return;
        }
        DBStore.getInstance(context).addAllLanguageInDB(properties);
        updateStateCity(context, "");
        return;
    }

    public LoginDetail validateUserAndPass(Context context, String username, String password, String type) {

        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("username", username));
        nameValuePairs.add(new BasicNameValuePair("password", password));
        nameValuePairs.add(new BasicNameValuePair("auth_type", type));

        LoginDetail loginResponse = new LoginDetail();
        DownloadResponse response = (DownloadResponse) RestClient.getInstance().makeHttpFormPostRequest(ProjectConstants.SERVER_URL + "user/generate_auth_cookie/", nameValuePairs, username);
        if (response.getResponseCode() == APIResponse.API_RESPONSE_SUCCESS) {
            Logger.log(TAG, "HTTP Request Successful ", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "-- parsing response --", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "Response: " + response.getdownloadResponse(), LogLevel.LOGGING_LEVEL_DEBUG);
            CustomJacksonObjectMapper mapper = new CustomJacksonObjectMapper();
            try {
                loginResponse = mapper.readValue(response.getdownloadResponse(), LoginDetail.class);
                updateUserSubscription(context,loginResponse.getUser_subscriptions());
                if (ProjectConstants.STATUS_OK.equalsIgnoreCase(loginResponse.getStatus())) {

                    DBStore.getInstance(context).addUserDataInDB(loginResponse);
                    try {

                        Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.SIGN_IN_TYPE_KEY, EncryptionHelper.encrypt(ProjectConstants.ENCRYPTION_KEY, type));
                        Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.USER_NAME_KEY, EncryptionHelper.encrypt(ProjectConstants.ENCRYPTION_KEY, username));
                        Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.PASSWORD_KEY, EncryptionHelper.encrypt(ProjectConstants.ENCRYPTION_KEY, password));
                        Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.COOKIE_KEY, EncryptionHelper.encrypt(ProjectConstants.ENCRYPTION_KEY, loginResponse.getCookie()));

                    } catch (Exception e) {

                        Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.SIGN_IN_TYPE_KEY, type);
                        Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.USER_NAME_KEY, username);
                        Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.PASSWORD_KEY, password);
                        Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.COOKIE_KEY, loginResponse.getCookie());
                    }
                } else {
                    return loginResponse;
                }
            } catch (JsonParseException e) {
                loginResponse.setStatus(ProjectConstants.STATUS_ERROR);
                loginResponse.setError(context.getResources().getString(R.string.parsing_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return loginResponse;
            } catch (JsonMappingException e) {
                loginResponse.setStatus(ProjectConstants.STATUS_ERROR);
                loginResponse.setError(context.getResources().getString(R.string.parsing_mapping_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return loginResponse;
            } catch (IOException e) {
                loginResponse.setStatus(ProjectConstants.STATUS_ERROR);
                loginResponse.setError(context.getResources().getString(R.string.parsing_IO_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return loginResponse;
            }
        }
        return loginResponse;
    }

    /**
     * authenticate media urls
     * @param context context of calling activity
     * @param username TE user name
     * @param file_url url to autheticate
     * @param movieId movie id of movie for which the url belongs to
     * @param trackLanguage language of movie selected
     * @param ismp3 if url is of mp3 or not
     * @return response in MediaUrlAuthResponse
     */
    public MediaUrlAuthResponse AuthenticateMediaUrl(Context context, String username,
                                                     String file_url,
                                                     long movieId,
                                                     String trackLanguage,
                                                     boolean ismp3) {

        if (username == null || file_url == null) {
            return null;
        }
        boolean isAssistiveEnable = Utility.getBooleanValueFromSharedPrefernce(context, ProjectConstants.ASSISTIVE_SWITCH_KEY);
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("file_url", file_url));
        if (isAssistiveEnable)  {
            nameValuePairs.add(new BasicNameValuePair("is_ada",""+isAssistiveEnable));
        }
        if (ismp3)  {
            nameValuePairs.add(new BasicNameValuePair("is_trackload",""+ismp3));
            nameValuePairs.add(new BasicNameValuePair("movie_id",""+movieId));
            nameValuePairs.add(new BasicNameValuePair("track_language",""+trackLanguage));
        }


        MediaUrlAuthResponse newUrlResponse = new MediaUrlAuthResponse();
        DownloadResponse response = (DownloadResponse) RestClient.getInstance().makeHttpFormPostRequest(ProjectConstants.SERVER_URL + "options/getValidUrl/", nameValuePairs, username,true);
        if (response.getResponseCode() == APIResponse.API_RESPONSE_SUCCESS) {
            Logger.log(TAG, "HTTP Request Successful ", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "-- parsing response --", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "Response: " + response.getdownloadResponse(), LogLevel.LOGGING_LEVEL_DEBUG);
            CustomJacksonObjectMapper mapper = new CustomJacksonObjectMapper();
            try {
                newUrlResponse = mapper.readValue(response.getdownloadResponse(), MediaUrlAuthResponse.class);
            } catch (JsonParseException e) {
                newUrlResponse.setStatus(ProjectConstants.STATUS_ERROR);
                newUrlResponse.setError(context.getResources().getString(R.string.parsing_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return newUrlResponse;
            } catch (JsonMappingException e) {
                newUrlResponse.setStatus(ProjectConstants.STATUS_ERROR);
                newUrlResponse.setError(context.getResources().getString(R.string.parsing_mapping_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return newUrlResponse;
            } catch (IOException e) {
                newUrlResponse.setStatus(ProjectConstants.STATUS_ERROR);
                newUrlResponse.setError(context.getResources().getString(R.string.parsing_IO_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return newUrlResponse;
            }
        }
        return newUrlResponse;
    }

    /**
     * get current location api request
     * @param context context of calling activity
     * @param latitude latitude
     * @param longitude langitude
     * @return return response in LocationResponse
     */
    public LocationResponse getCurrentLocation(Context context, double latitude, double longitude) {

        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("latitude", "" + latitude));
        nameValuePairs.add(new BasicNameValuePair("longitude", "" + longitude));


        LocationResponse locationResponse = new LocationResponse();
        DownloadResponse response = (DownloadResponse) RestClient.getInstance().makeHttpFormPostRequest(ProjectConstants.SERVER_URL + "options/getGeographicLocations/", nameValuePairs, null);
        if (response.getResponseCode() == APIResponse.API_RESPONSE_SUCCESS) {
            Logger.log(TAG, "HTTP Request Successful ", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "-- parsing response --", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "Response: " + response.getdownloadResponse(), LogLevel.LOGGING_LEVEL_DEBUG);
            CustomJacksonObjectMapper mapper = new CustomJacksonObjectMapper();
            try {
                locationResponse = mapper.readValue(response.getdownloadResponse(), LocationResponse.class);
            } catch (JsonParseException e) {
                locationResponse.setStatus(ProjectConstants.STATUS_ERROR);
                locationResponse.setError(context.getResources().getString(R.string.parsing_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return locationResponse;
            } catch (JsonMappingException e) {
                locationResponse.setStatus(ProjectConstants.STATUS_ERROR);
                locationResponse.setError(context.getResources().getString(R.string.parsing_mapping_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return locationResponse;
            } catch (IOException e) {
                locationResponse.setStatus(ProjectConstants.STATUS_ERROR);
                locationResponse.setError(context.getResources().getString(R.string.parsing_IO_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return locationResponse;
            }
        }
        return locationResponse;
    }


    //	public static String copyFileUsingStream(Context context,String source) throws IOException {
    //
    //		String path = "track" + System.currentTimeMillis() +".mp3";
    //
    //		FileOutputStream fos = context.openFileOutput(path, Context.MODE_PRIVATE);
    //		InputStream is = null;
    //		try {
    //			is = new FileInputStream(source);
    //			byte[] buffer = new byte[1024];
    //			int length;
    //			while ((length = is.read(buffer)) > 0) {
    //				fos.write(buffer, 0, length);
    //			}
    //		} catch(Exception ex)	{
    //			path = null;
    //		}
    //		finally {
    //			is.close();
    //			fos.close();
    //		}
    //		Utility.deleteAudioFromDevice(new File(source));
    //		return path;
    //	}


    /**
     * check and update downloaded movies api request
     * @param context context of calling activity
     * @return response in MoviePost
     */
    public MoviePost checkAndUpdateDownloadedMovies(final Context context) {

        MoviePost movieTitle = null;

        HashMap<Long, ArrayList<Long>> downloadingMovie = DBStore.getInstance(context).getDownloadingMovies(null);
        DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);

        if (downloadingMovie != null && downloadingMovie.size() > 0) {
            for (HashMap.Entry<Long, ArrayList<Long>> entry : downloadingMovie.entrySet()) {
                Long movieId = entry.getKey();
                ArrayList<Long> preference = entry.getValue();
                if (preference != null && preference.size() > 1) {
                    DownloadManager.Query firstStatusQuery = new DownloadManager.Query();
                    firstStatusQuery.setFilterById(preference.get(0));

                    DownloadManager.Query secondStatusQuery = new DownloadManager.Query();
                    secondStatusQuery.setFilterById(preference.get(1));

                    Cursor firstCursor = manager.query(firstStatusQuery);
                    //					firstCursor.moveToFirst();

                    Cursor secondCursor = manager.query(secondStatusQuery);
                    //					secondCursor.moveToFirst();
                    if (firstCursor.moveToFirst() && secondCursor.moveToFirst()) {

                        if (firstCursor.getInt(secondCursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL &&
                                secondCursor.getInt(secondCursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                            MoviePost movie = DBStore.getInstance(context).getMovie(movieId);
                            MovieInDownLoad movieInDwnld = DBStore.getInstance(context).getMovieInDownload(movieId);

                            if (movie != null) {
                                String firstPath = null;
                                String secondPath = null;


                                long startTime = System.currentTimeMillis();
                                Utility.copyFileToPrivate(context,firstCursor.getString(firstCursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME)),
                                        "te_" + movieId + "_" +movieInDwnld.getMovieDownloadingLanguage()+ ".mp3");
                                Utility.copyFileToPrivate(context,secondCursor.getString(secondCursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME)),"te_"
                                        + movieId + "_" +movieInDwnld.getMovieDownloadingLanguage()+ ".fpi");

                                Log.d("[TECOPY]", "Time to copy : " + (System.currentTimeMillis() - startTime));

                                firstPath = context.getFilesDir().getAbsolutePath() + "/" + "te_" + movieId + "_"+ movieInDwnld.getMovieDownloadingLanguage() + ".mp3";
                                secondPath = context.getFilesDir().getAbsolutePath() + "/" + "te_" + movieId+ "_" + movieInDwnld.getMovieDownloadingLanguage() + ".fpi";


                                if (firstPath != null && secondPath != null) {


                                    final String srtFileUniqueKey = context.getFilesDir().getAbsolutePath() + "/" + movieId + "_" + movieInDwnld.getMovieDownloadingLanguage() + "_" + System.currentTimeMillis() + "_movie_subtitles.srt";
                                    srtFileUrl = movieInDwnld.getSrtUrl();
                                    String indexCheckSum = movieInDwnld.getIndexChecksum();
                                    String trackCheckSum = movieInDwnld.getTrackChecksum();
                                    String resultTrackChecksum = Utility.getMD5Checksum(firstPath);
                                    String resultIndexChecksum = Utility.getMD5Checksum(secondPath);



                                    if (indexCheckSum != null && indexCheckSum.equals(resultIndexChecksum)
                                            && trackCheckSum != null && trackCheckSum.equals(resultTrackChecksum)) {



                                        movie.setNativePath(firstPath);
                                        movie.setIndexPath(secondPath);
                                        movie.setTheaterLat(movieInDwnld.getTheaterLat());
                                        movie.setTheaterLong(movieInDwnld.getTheaterLong());
                                        movie.setMovieDownloadedLanguage(movieInDwnld.getMovieDownloadingLanguage());
                                        movie.setDownloadedOnTimeStamp(System.currentTimeMillis());
//										movie.setAudio_type_downloaded(movieInDwnld.getAudioTypeSelected());
                                        if (srtFileUrl != null && srtFileUrl.length() > 1) {
                                            movie.setSrtFilePath(srtFileUniqueKey);
                                        } else {
                                            movie.setSrtFilePath("");
                                        }
                                        DBStore.getInstance(context).addDownloadedMovieInDB(movie);
                                        DBStore.getInstance(context).deleteDownlodingTask(movieId);
                                        updateMovieStatusRequest(context,movieId,movieInDwnld.getMovieDownloadingLanguage(),ProjectConstants.MOVIE_STATUS_DOWNLOADED);
                                        Thread thread = new Thread() {
                                            public void run() {
                                                downloadSrtFile(context, srtFileUrl, srtFileUniqueKey);
                                            }
                                        };
                                        thread.start();

                                        movieTitle = movie;
                                    } else {
                                        firstCursor.close();
                                        secondCursor.close();
                                        DBStore.getInstance(context).deleteDownlodingTask(movieId);


                                        // Get a handler that can be used to post to the main thread
                                        Handler mainHandler = new Handler(context.getMainLooper());
                                        Runnable myRunnable = new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(context, "Fails to download complete movie Please try later", Toast.LENGTH_LONG).show();
                                            }
                                        };
                                        mainHandler.post(myRunnable);
                                    }


                                }
                                firstCursor.close();
                                secondCursor.close();
                            } else {
                                firstCursor.close();
                                secondCursor.close();
                            }
                        }
                    } else {
                        if (firstCursor != null) {
                            firstCursor.close();
                        }
                        if (secondCursor != null) {
                            secondCursor.close();
                        }
                    }
                }
            }
            return movieTitle;
        } else {
            return movieTitle;
        }
    }

    /**
     * update movies and theater data
     * @param context context of calling activity
     * @param userName TE user name
     * @return response in TheaterResponse
     */
    public TheaterResponse updateMovieAndTheaterData(Context context, String userName) {
        TheaterResponse theaterResponse = new TheaterResponse();
        String location = getAddressFromLocation(context);
        if (location != null && location.length() > 0) {
            theaterResponse = updateTheaters(context, location, userName);
            if (ProjectConstants.STATUS_OK.equalsIgnoreCase(theaterResponse.getStatus())) {
                // send movie request
                if (theaterResponse != null && theaterResponse.getPosts() != null && theaterResponse.getPosts().size() > 0) {
                    MovieResponse movieResponse = updateMovie(context, theaterResponse.getPosts(), userName);
                    if (ProjectConstants.STATUS_OK.equalsIgnoreCase(movieResponse.getStatus())) {
                        return theaterResponse;
                    } else {
                        theaterResponse.setStatus(ProjectConstants.STATUS_WITH_MESSAGE);
                        theaterResponse.setError(context.getResources().getString(R.string.movie_error_msg) + " " + movieResponse.getError());
                        return theaterResponse;
                    }
                } else {
                    DBStore.getInstance(context).clearMovieDataTable();
                    return theaterResponse;
                }
            } else {
                return theaterResponse;
            }


        } else {
            theaterResponse.setStatus(ProjectConstants.STATUS_ERROR);
            theaterResponse.setError(context.getResources().getString(R.string.location_error_msg));
            return theaterResponse;
        }
    }

    /**
     * method to downlaod the sync mp3 background
     * @param context context of calling activity
     * @param locale current selected locale for TE
     */
    private void downloadSyncMp3(Context context,String locale) {
        String syncUrl;
        String filePath ;
        if (locale.equalsIgnoreCase("es")) {
            syncUrl = ProjectConstants.SYNC_TUTORIAL_SPANISH_URL;
            filePath = context.getFilesDir().getAbsolutePath() + "/ track_sync_tutorial_es.mp3";
        } else {
            syncUrl = ProjectConstants.SYNC_TUTORIAL_ENGLISH_URL;
            filePath = context.getFilesDir().getAbsolutePath() + "/ track_sync_tutorial_en.mp3";
        }

        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(syncUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                if (locale.equalsIgnoreCase("es")) {
                    Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.SYNC_IN_PROGRESS_MP3_LOCAL_PATH_SPANISH, null);
                } else {
                    Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.SYNC_IN_PROGRESS_MP3_LOCAL_PATH_ENGLISH, null);
                }
                return;
            }

            // this will be useful to display download percentage
            // might be -1: server did not report the length
            //            int fileLength = connection.getContentLength();

            // download the file
            input = connection.getInputStream();
            output = new FileOutputStream(filePath);

            byte data[] = new byte[4096];
            int count;
            while ((count = input.read(data)) != -1) {
                output.write(data, 0, count);
            }

        } catch (Exception e) {
            if (locale.equalsIgnoreCase("es")) {
                Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.SYNC_IN_PROGRESS_MP3_LOCAL_PATH_SPANISH, null);
            } else {
                Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.SYNC_IN_PROGRESS_MP3_LOCAL_PATH_ENGLISH, null);
            }
            return;
        } finally {
            try {
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
                return;
            }

            if (connection != null)
                connection.disconnect();
        }

        if (locale.equalsIgnoreCase("es")) {
            Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.SYNC_IN_PROGRESS_MP3_LOCAL_PATH_SPANISH, filePath);
        } else {
            Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.SYNC_IN_PROGRESS_MP3_LOCAL_PATH_ENGLISH, filePath);
        }
        return;
    }


    /**
     * method to downlaod the srt file
     * @param context context of calling activity
     * @param fileUrl srt url
     * @param filePath file path where to store the srt
     */
    private void downloadSrtFile(Context context, String fileUrl, String filePath) {

        if (fileUrl == null || fileUrl.length() == 0 || filePath == null || filePath.length() == 0) {

            return;
        }

        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(fileUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            System.out.println(".....Downloading srt...  " + fileUrl);

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return;
            }

            // this will be useful to display download percentage
            // might be -1: server did not report the length
            //            int fileLength = connection.getContentLength();

            // download the file
            input = connection.getInputStream();
            output = new FileOutputStream(filePath);

            byte data[] = new byte[4096];
            int count;
            while ((count = input.read(data)) != -1) {
                output.write(data, 0, count);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return;
        } finally {
            try {
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
                return;
            }

            if (connection != null)
                connection.disconnect();
        }
        return;
    }


    /**
     * validate and update the login data of user
     * @param context context of calling activity
     * @param username TE username
     * @param password TE userpassword
     * @param type login type selected by user
     * @return response in LoginDetail
     */
    public LoginDetail validateAndUpdateData(Context context, String username, String password, String type) {
        registerDevice(context);
        getPreferedLanguage(context);
        downloadSyncMp3(context, "es");
        downloadSyncMp3(context,"en");


        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("username", username));
        nameValuePairs.add(new BasicNameValuePair("password", password));
        nameValuePairs.add(new BasicNameValuePair("auth_type", type));


        LoginDetail loginResponse = new LoginDetail();
        DownloadResponse response = (DownloadResponse) RestClient.getInstance().makeHttpFormPostRequest(ProjectConstants.SERVER_URL + "user/generate_auth_cookie/", nameValuePairs, username);
        if (response.getResponseCode() == APIResponse.API_RESPONSE_SUCCESS) {
            Logger.log(TAG, "HTTP Request Successful ", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "-- parsing response --", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "Response: " + response.getdownloadResponse(), LogLevel.LOGGING_LEVEL_DEBUG);
            CustomJacksonObjectMapper mapper = new CustomJacksonObjectMapper();
            try {
                loginResponse = mapper.readValue(response.getdownloadResponse(), LoginDetail.class);
                updateUserSubscription(context,loginResponse.getUser_subscriptions());
                if (ProjectConstants.STATUS_OK.equalsIgnoreCase(loginResponse.getStatus())) {
                    try {
                        DBStore.getInstance(context).addUserDataInDB(loginResponse);
                        Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.SIGN_IN_TYPE_KEY, EncryptionHelper.encrypt(ProjectConstants.ENCRYPTION_KEY, type));
                        Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.USER_NAME_KEY, EncryptionHelper.encrypt(ProjectConstants.ENCRYPTION_KEY, username));
                        Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.PASSWORD_KEY, EncryptionHelper.encrypt(ProjectConstants.ENCRYPTION_KEY, password));
                        Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.COOKIE_KEY, EncryptionHelper.encrypt(ProjectConstants.ENCRYPTION_KEY, loginResponse.getCookie()));

                        // api for advertisement data
                        getAdvertisement(context, username);


                    } catch (Exception e) {
                        Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.SIGN_IN_TYPE_KEY, type);
                        Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.USER_NAME_KEY, username);
                        Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.PASSWORD_KEY, password);
                        Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.COOKIE_KEY, loginResponse.getCookie());
                    }
                } else {
                    return loginResponse;
                }
            } catch (JsonParseException e) {
                loginResponse.setStatus(ProjectConstants.STATUS_ERROR);
                loginResponse.setError(context.getResources().getString(R.string.parsing_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return loginResponse;
            } catch (JsonMappingException e) {
                loginResponse.setStatus(ProjectConstants.STATUS_ERROR);
                loginResponse.setError(context.getResources().getString(R.string.parsing_mapping_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return loginResponse;
            } catch (IOException e) {
                loginResponse.setStatus(ProjectConstants.STATUS_ERROR);
                loginResponse.setError(context.getResources().getString(R.string.parsing_IO_fail_error));
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                return loginResponse;
            }

        }
        return loginResponse;
    }

    /**
     * register device with GCM
     * @param context context of calling activity
     * @return response in DeviceRegWithGCM
     */
    private static DeviceRegWithGCM SendRegistrationIDToServer(Context context) {

        String deviceID = Utility.getDeviceID(context);
        DeviceRegWithGCM regWithGCM = new DeviceRegWithGCM();
        regWithGCM.setDevice_id(deviceID);
        regWithGCM.setDevice_os("android");
        regWithGCM.setIs_ada("0");
        regWithGCM.setRegistration_id(regId);


        StringBuilder requestURL = new StringBuilder("http://admin-dev.theaterears.net/api/options/register_notification_devices/?");
        requestURL.append("device_id=" + regWithGCM.getDevice_id());
        requestURL.append("&registration_id=" + regWithGCM.getRegistration_id());
        requestURL.append("&device_os=" + regWithGCM.getDevice_os());
        requestURL.append("&is_ada=" + regWithGCM.getIs_ada());


        DeviceRegWithGCM registrationResponse = new DeviceRegWithGCM();
        DownloadResponse response = (DownloadResponse) RestClient.getInstance().makeHttpGetRequest(requestURL.toString(), "");
        if (response.getResponseCode() == APIResponse.API_RESPONSE_SUCCESS) {
            Logger.log(TAG, "HTTP Request Successful ", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "-- parsing response --", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "Response: " + response.getdownloadResponse(), LogLevel.LOGGING_LEVEL_DEBUG);

            Utility.saveBooleanValueInSharedPrefrence(context, ProjectConstants.IS_DEVICE_REGISTERED_WITH_GCM, true);
            Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.DEVICE_REGISTRATION_ID_WITH_GCM, regId);

            System.out.println("!!!!@@@@device registerted successfully and regid saved in sharedpreferences%%%%$$$$ = " + regId);
        } else {
            Utility.saveBooleanValueInSharedPrefrence(context, ProjectConstants.IS_DEVICE_REGISTERED_WITH_GCM, false);
            return registrationResponse;
        }
        return registrationResponse;
    }

    /**
     * method used to invoke the device registration request
     * @param context context of calling activity
     */
    private static void RegisterDeviceWithGCM(Context context) {
        if (checkPlayServices(context)) {
            if (Utility.isMobileConnected(context) || Utility.isWifiConnected(context)) {
                registerInBackground(context);
            }
        }
    }

    /**
     * invoke async task to start the device registration process in background
     * @param context context of calling activity
     */
    private static void registerInBackground(final Context context) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    if (gcmObj == null) {
                        gcmObj = GoogleCloudMessaging.getInstance(context);
                    }
                    regId = gcmObj.register(ProjectConstants.GOOGLE_PROJ_ID);
                    if (regId != null && regId.length() > 0) {
                        SendRegistrationIDToServer(context);

                    }
                    Log.d("REGISTRATION ID", regId);

                } catch (IOException ex) {
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void params) {
            }
        }.execute(null, null, null);
    }


    /**
     * check id play services are enable on the device
     * @param context context of calling activity
     * @return true if enabled otherwise false
     */
    private static boolean checkPlayServices(Context context) {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                //				GooglePlayServicesUtil.getErrorDialog(resultCode, context,PLAY_SERVICES_RESOLUTION_REQUEST).show();
                //TODO : fire broadcast to open error dialog
            } else {

            }
            return false;
        }
        return true;
    }

//    public MovieResponse getMovieFromId(Context context, long movieId, String userName, String postType) {
//
//        Log.d("TE[GCM]", "getting mvie from id");
//
////		MoviePost post = DBStore.getInstance(context).getMovie(movieId);
////
////		if (post != null && post.getId() > 0)	{
////			return null;
////		}
//
//        if (userName == null || userName.length() == 0) {
//            return null;
//        }
//
//        StringBuilder stringBuilder = new StringBuilder(ProjectConstants.SERVER_URL);
//        stringBuilder.append("get_post/?nopaging=1&post_id=");
//        stringBuilder.append(movieId);
//        stringBuilder.append("&post_type=");
//        stringBuilder.append(postType);
//
//        MovieResponse movieResponse = new MovieResponse();
//        DownloadResponse response = (DownloadResponse) RestClient.getInstance().makeHttpGetRequest(stringBuilder.toString(), userName);
//        if (response.getResponseCode() == APIResponse.API_RESPONSE_SUCCESS) {
//            Logger.log(TAG, "HTTP Request Successful ", LogLevel.LOGGING_LEVEL_DEBUG);
//            Logger.log(TAG, "-- parsing response --", LogLevel.LOGGING_LEVEL_DEBUG);
//            Logger.log(TAG, "Response: " + response.getdownloadResponse(), LogLevel.LOGGING_LEVEL_DEBUG);
//            CustomJacksonObjectMapper mapper = new CustomJacksonObjectMapper();
//            Log.d("TE[GCM]", "resposne success");
//
//            try {
//                movieResponse = mapper.readValue(response.getdownloadResponse(), MovieResponse.class);
//            } catch (JsonParseException e) {
//                movieResponse.setStatus(ProjectConstants.STATUS_ERROR);
//                movieResponse.setError(context.getResources().getString(R.string.parsing_fail_error));
//                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
//                Log.d("TE[GCM]", "JsonParseException");
//
//                return movieResponse;
//            } catch (JsonMappingException e) {
//                movieResponse.setStatus(ProjectConstants.STATUS_ERROR);
//                movieResponse.setError(context.getResources().getString(R.string.parsing_mapping_fail_error));
//                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
//                Log.d("TE[GCM]", "JsonMappingException");
//
//                return movieResponse;
//            } catch (IOException e) {
//                movieResponse.setStatus(ProjectConstants.STATUS_ERROR);
//                movieResponse.setError(context.getResources().getString(R.string.parsing_IO_fail_error));
//                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
//                Log.d("TE[GCM]", "IOException");
//                return movieResponse;
//            }
//        } else {
//            Log.d("TE[GCM]", "resposne fail");
//
//            movieResponse.setStatus(ProjectConstants.STATUS_ERROR);
//            movieResponse.setError(">HHTP request Falure");
//            return movieResponse;
//        }
//        return movieResponse;
//    }

    /**
     * check payment api request
     * @param context context of calling activity
     * @param movieId id of movie for which the status has to be updated
     * @param userId id of TE user
     * @param userName TE username
     * @param languageId language id of movie selected
     * @return
     */
    public CheckPaymentResponse checkPaymentApiRequest(Context context, long movieId, long userId , String userName,String languageId) {

        String deviceId = Utility.getDeviceID(context);

//        StringBuilder stringBuilder = new StringBuilder(ProjectConstants.SERVER_URL);
//        stringBuilder.append(ProjectConstants.CHECK_PAYMENT_BASIC_URL);
//        stringBuilder.append("device_id=" + deviceId);
//        stringBuilder.append("&movie_id=" + movieId);
//        stringBuilder.append("&user_id=" + userId);

        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("device_id", deviceId));
        nameValuePairs.add(new BasicNameValuePair("movie_id", ""+movieId));
        nameValuePairs.add(new BasicNameValuePair("user_id", ""+userId));
        nameValuePairs.add(new BasicNameValuePair("language_id", ""+languageId));


        CheckPaymentResponse paymentResponse = new CheckPaymentResponse();
        DownloadResponse response = (DownloadResponse) RestClient.getInstance().makeHttpFormPostRequest(ProjectConstants.SERVER_URL + ProjectConstants.CHECK_PAYMENT_BASIC_URL,nameValuePairs,userName);
        if (response.getResponseCode() == APIResponse.API_RESPONSE_SUCCESS) {
            Logger.log(TAG, "HTTP Request Successful ", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "-- parsing response --", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "Response: " + response.getdownloadResponse(), LogLevel.LOGGING_LEVEL_DEBUG);
            CustomJacksonObjectMapper mapper = new CustomJacksonObjectMapper();
            Log.d("TE[GCM]", "resposne success");

            try {
                paymentResponse = mapper.readValue(response.getdownloadResponse(), CheckPaymentResponse.class);
                updateUserSubscription(context,paymentResponse.getUser_subscriptions());
            } catch (JsonParseException e) {
                paymentResponse.setStatus(ProjectConstants.STATUS_ERROR);
                paymentResponse.setSuccess(false);
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                Log.d("TE[GCM]", "JsonParseException");

                return paymentResponse;
            } catch (JsonMappingException e) {
                paymentResponse.setStatus(ProjectConstants.STATUS_ERROR);
                paymentResponse.setSuccess(false);
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                Log.d("TE[GCM]", "JsonMappingException");

                return paymentResponse;
            } catch (IOException e) {
                paymentResponse.setStatus(ProjectConstants.STATUS_ERROR);
                paymentResponse.setSuccess(false);
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                Log.d("TE[GCM]", "IOException");
                return paymentResponse;
            }
        } else {
            Log.d("TE[GCM]", "resposne fail");

            paymentResponse.setStatus(ProjectConstants.STATUS_ERROR);
            paymentResponse.setSuccess(false);
            return paymentResponse;
        }
        return paymentResponse;
    }

    private void updateUserSubscription(Context context,ArrayList<UserSubscriptions> userSubscriptionses)   {
        if (userSubscriptionses != null && userSubscriptionses.size() > 0)  {
            String deviceId = Utility.getDeviceID(context);
            for (UserSubscriptions subscription : userSubscriptionses)  {
                if (subscription.getDevice_id().equalsIgnoreCase(deviceId)) {
                    Utility.saveLongValueInSharedPrefrence(context,ProjectConstants.USER_SUBSCRIPTION_ID_KEY,subscription.getSubscription_id());
                    Utility.saveIntValueInSharedPrefrence(context, ProjectConstants.USER_SUBSCRIPTION_AVAILABLE_COUNT, subscription.getSubscription_available_count());
                    Utility.saveLongValueInSharedPrefrence(context, ProjectConstants.USER_SUBSCRIPTION_END_DATE, subscription.getSubscription_end_date());
                    Utility.saveIntValueInSharedPrefrence(context, ProjectConstants.USER_SUBSCRIPTION_TOTAL_COUNT, subscription.getSubscription_total_count());
                    Utility.saveLongValueInSharedPrefrence(context, ProjectConstants.USER_SUBSCRIPTION_PURCHASE_DATE, subscription.getPurchase_date());
                    Utility.saveStringValueInSharedPrefrence(context, ProjectConstants.USER_SUBSCRIPTION_TYPE, subscription.getSubscription_type());

                    break;
                }
            }
        } else  {
            Utility.saveLongValueInSharedPrefrence(context, ProjectConstants.USER_SUBSCRIPTION_ID_KEY, 0);
            Utility.saveIntValueInSharedPrefrence(context, ProjectConstants.USER_SUBSCRIPTION_AVAILABLE_COUNT, 0);
            Utility.saveLongValueInSharedPrefrence(context, ProjectConstants.USER_SUBSCRIPTION_END_DATE, 0);
            Utility.saveIntValueInSharedPrefrence(context,ProjectConstants.USER_SUBSCRIPTION_TOTAL_COUNT,0);
            Utility.saveLongValueInSharedPrefrence(context, ProjectConstants.USER_SUBSCRIPTION_PURCHASE_DATE, 0);
            Utility.saveStringValueInSharedPrefrence(context,ProjectConstants.USER_SUBSCRIPTION_TYPE,null);

        }
    }

    /**
     * Initialize the payment of movie
     * @param context context of calling activity
     * @param productId product id to purchase
     * @param moviePrice price of movie to purchase
     * @param product_type type of product to purchase
     * @param couponId id of coupon if applied
     * @param cardType type of card used for transaction
     * @return response in InitializePaymentResponse
     */
    public InitializePaymentResponse initPaymentApiRequest(Context context, String productId, String moviePrice, String product_type,long couponId,String cardType) {
        LoginDetail userDetail = DBStore.getInstance(context).getUserLogInDetail();

        String deviceId = Utility.getDeviceID(context);

        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("device_id", deviceId));
        nameValuePairs.add(new BasicNameValuePair("product_id", "" + productId));
        nameValuePairs.add(new BasicNameValuePair("product_type", "" + product_type));
        nameValuePairs.add(new BasicNameValuePair("transaction_value", "" + moviePrice));
        nameValuePairs.add(new BasicNameValuePair("device_os", "" + "android"));
        nameValuePairs.add(new BasicNameValuePair("user_id", "" + userDetail.getId()));
        if (couponId > 0)   {
            nameValuePairs.add(new BasicNameValuePair("coupon_id", "" + couponId));
        }
        if (cardType != null)   {
            nameValuePairs.add(new BasicNameValuePair("payment_card_type", cardType));
        }

        InitializePaymentResponse initPaymentResponse = new InitializePaymentResponse();
        DownloadResponse response = (DownloadResponse) RestClient.getInstance().makeHttpFormPostRequest(ProjectConstants.SERVER_URL+ProjectConstants.INITIALIZE_PAYMENT_BASIC_URL,nameValuePairs,userDetail.getEmail());
        if (response.getResponseCode() == APIResponse.API_RESPONSE_SUCCESS) {
            Logger.log(TAG, "HTTP Request Successful ", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "-- parsing response --", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "Response: " + response.getdownloadResponse(), LogLevel.LOGGING_LEVEL_DEBUG);
            CustomJacksonObjectMapper mapper = new CustomJacksonObjectMapper();
            Log.d("TE[GCM]", "resposne success");

            try {
                initPaymentResponse = mapper.readValue(response.getdownloadResponse(), InitializePaymentResponse.class);

            } catch (JsonParseException e) {
                initPaymentResponse.setStatus(ProjectConstants.STATUS_ERROR);
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                Log.d("TE[GCM]", "JsonParseException");

                return initPaymentResponse;
            } catch (JsonMappingException e) {
                initPaymentResponse.setStatus(ProjectConstants.STATUS_ERROR);
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                Log.d("TE[GCM]", "JsonMappingException");

                return initPaymentResponse;
            } catch (IOException e) {
                initPaymentResponse.setStatus(ProjectConstants.STATUS_ERROR);
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                Log.d("TE[GCM]", "IOException");
                return initPaymentResponse;
            }
        } else {
            Log.d("TE[GCM]", "resposne fail");

            initPaymentResponse.setStatus(ProjectConstants.STATUS_ERROR);
            return initPaymentResponse;
        }
        return initPaymentResponse;
    }

    /**
     * coupon response api to redeem coupon
     * @param context context of callling activity
     * @param movieId id of movie for which coupoon is applied
     * @param languageId language of movie
     * @param couponCode coupon code to app;y
     * @return response of CouponResponse
     */
    public CouponResponse redeemCoupon(Context context,long movieId, String languageId,String couponCode) {
        LoginDetail userDetail = DBStore.getInstance(context).getUserLogInDetail();

        String deviceId = Utility.getDeviceID(context);

        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("user_id", "" + userDetail.getId()));
        nameValuePairs.add(new BasicNameValuePair("movie_id", "" + movieId));
        nameValuePairs.add(new BasicNameValuePair("device_id", deviceId));
        nameValuePairs.add(new BasicNameValuePair("language_id", "" + languageId));
        nameValuePairs.add(new BasicNameValuePair("coupon_code", "" + couponCode));


        CouponResponse couponResponse = new CouponResponse();
        DownloadResponse response = (DownloadResponse) RestClient.getInstance().makeHttpFormPostRequest(ProjectConstants.SERVER_URL+ProjectConstants.REDEEM_COUPON_API,nameValuePairs,userDetail.getEmail());
        if (response.getResponseCode() == APIResponse.API_RESPONSE_SUCCESS) {
            Logger.log(TAG, "HTTP Request Successful ", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "-- parsing response --", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "Response: " + response.getdownloadResponse(), LogLevel.LOGGING_LEVEL_DEBUG);
            CustomJacksonObjectMapper mapper = new CustomJacksonObjectMapper();
            Log.d("TE[GCM]", "resposne success");

            try {
                couponResponse = mapper.readValue(response.getdownloadResponse(), CouponResponse.class);

            } catch (JsonParseException e) {
                couponResponse.setStatus(ProjectConstants.STATUS_ERROR);
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                Log.d("TE[GCM]", "JsonParseException");

                return couponResponse;
            } catch (JsonMappingException e) {
                couponResponse.setStatus(ProjectConstants.STATUS_ERROR);
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                Log.d("TE[GCM]", "JsonMappingException");

                return couponResponse;
            } catch (IOException e) {
                couponResponse.setStatus(ProjectConstants.STATUS_ERROR);
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                Log.d("TE[GCM]", "IOException");
                return couponResponse;
            }
        } else {
            Log.d("TE[GCM]", "resposne fail");

            couponResponse.setStatus(ProjectConstants.STATUS_ERROR);
            return couponResponse;
        }
        return couponResponse;
    }


    /**
     * request for updating the paymetn api
     * @param context context of calling activity
     * @param movieId id of movie to update the payment status
     * @param transaction_id transction id of payment
     * @param gatewayTrasictionId gatewayTrasictionId of payment
     * @param languageId language id of movie
     * @param paymentMethodToken paymentMethodToken of payment
     * @param paymentStatus status of payment
     * @param message message received for payment gateway
     * @param saveCard card detais
     * @return response of UpdatePaymentResponse
     */
    public UpdatePaymentResponse updatePaymentApiRequest(Context context, long movieId, String transaction_id,
                                                         String gatewayTrasictionId,String languageId,
                                                         String paymentMethodToken,String paymentStatus,String message,
                                                         boolean saveCard) {

        LoginDetail userDetail = DBStore.getInstance(context).getUserLogInDetail();

        StringBuilder builder = new StringBuilder(ProjectConstants.SERVER_URL+ProjectConstants.UPDATE_PAYMENT_BASIC_URL);
        builder.append("transaction_id="+transaction_id);
        builder.append("&payment_status="+paymentStatus);
        builder.append("&result_code="+"NA");
        builder.append("&message="+message);
        if (gatewayTrasictionId != null && gatewayTrasictionId.length() > 0) {
            builder.append("&reference_transaction_id=" + gatewayTrasictionId);
        } else  {
            builder.append("&reference_transaction_id=NA");

        }
        builder.append("&movie_id="+movieId);
        builder.append("&language_id="+languageId);
        if (paymentMethodToken != null && paymentMethodToken.length() > 0)  {
            builder.append("&payment_method_token="+paymentMethodToken);

        }
        builder.append("&save_card=" + saveCard);



        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("transaction_id", transaction_id));
        nameValuePairs.add(new BasicNameValuePair("payment_status", "" + paymentStatus));
        nameValuePairs.add(new BasicNameValuePair("result_code", "" + "NA"));
        nameValuePairs.add(new BasicNameValuePair("message", "" + message));
        nameValuePairs.add(new BasicNameValuePair("movie_id", "" + movieId));
        nameValuePairs.add(new BasicNameValuePair("language_id", "" + languageId));
        if (gatewayTrasictionId != null && gatewayTrasictionId.length() > 0) {
            nameValuePairs.add(new BasicNameValuePair("reference_transaction_id",gatewayTrasictionId));
        } else  {
            nameValuePairs.add(new BasicNameValuePair("reference_transaction_id","NA"));

        }
        if (paymentMethodToken != null && paymentMethodToken.length() > 0)  {
            nameValuePairs.add(new BasicNameValuePair("payment_method_token",paymentMethodToken));
        }
        nameValuePairs.add(new BasicNameValuePair("save_card", ""+saveCard));


        UpdatePaymentResponse updateResponse = new UpdatePaymentResponse();
        DownloadResponse response = (DownloadResponse) RestClient.getInstance().makeHttpFormPostRequest(ProjectConstants.SERVER_URL+ProjectConstants.UPDATE_PAYMENT_BASIC_URL,nameValuePairs,userDetail.getEmail());
        if (response.getResponseCode() == APIResponse.API_RESPONSE_SUCCESS) {
            Logger.log(TAG, "HTTP Request Successful ", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "-- parsing response --", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "Response: " + response.getdownloadResponse(), LogLevel.LOGGING_LEVEL_DEBUG);
            CustomJacksonObjectMapper mapper = new CustomJacksonObjectMapper();
            Log.d("TE[GCM]", "response success");

            try {
                updateResponse = mapper.readValue(response.getdownloadResponse(), UpdatePaymentResponse.class);
                updateUserSubscription(context,updateResponse.getUser_subscriptions());
            } catch (JsonParseException e) {
                updateResponse.setStatus(ProjectConstants.STATUS_ERROR);
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                updateResponse.setSuccess(false);
                Log.d("TE[GCM]", "JsonParseException");

            } catch (JsonMappingException e) {
                updateResponse.setStatus(ProjectConstants.STATUS_ERROR);
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                updateResponse.setSuccess(false);
                Log.d("TE[GCM]", "JsonMappingException");

            } catch (IOException e) {
                updateResponse.setStatus(ProjectConstants.STATUS_ERROR);
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                updateResponse.setSuccess(false);
                Log.d("TE[GCM]", "IOException");
            }
        } else {
            Log.d("TE[GCM]", "resposne fail");

            updateResponse.setStatus(ProjectConstants.STATUS_ERROR);
            updateResponse.setSuccess(false);
        }

        if(ProjectConstants.STATUS_ERROR.equalsIgnoreCase(updateResponse.getStatus())) {
            if(updatePaymentFailCount <= 5) {
                updatePaymentFailCount++;
                updatePaymentApiRequest(context, movieId, transaction_id,gatewayTrasictionId,languageId,paymentMethodToken,paymentStatus,message,saveCard);
            } else {
                DBStore.getInstance(context).addUpdateFailRequestInDB(movieId , builder.toString());
                updatePaymentFailCount =0;
            }
        }
        return updateResponse;
    }

    /**
     * request to update movie status after sync
     * @param context context of calling activity
     * @param movieId id of movie to update
     * @param languageId language id of movie to update
     * @param movieStatus mvovie status
     * @return responce in MovieStatusResponse
     */
    public MovieStatusResponse updateMovieStatusRequest(Context context, long movieId, String languageId ,String movieStatus) {

        LoginDetail userDetail = DBStore.getInstance(context).getUserLogInDetail();

        StringBuilder builder = new StringBuilder(ProjectConstants.SERVER_URL+ProjectConstants.UPDATE_MOVIE_STATUS_URL);
        builder.append("?user_id=" + userDetail.getId());
        builder.append("&movie_id=" + movieId);
        builder.append("&device_id=" + Utility.getDeviceID(context));
        builder.append("&language_id=" + languageId);
        builder.append("&movie_status=" + movieStatus);


        MovieStatusResponse updateResponse = new MovieStatusResponse();
        DownloadResponse response = (DownloadResponse) RestClient.getInstance().makeHttpGetRequest(builder.toString(),userDetail.getEmail());
        if (response.getResponseCode() == APIResponse.API_RESPONSE_SUCCESS) {
            Logger.log(TAG, "HTTP Request Successful ", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "-- parsing response --", LogLevel.LOGGING_LEVEL_DEBUG);
            Logger.log(TAG, "Response: " + response.getdownloadResponse(), LogLevel.LOGGING_LEVEL_DEBUG);
            CustomJacksonObjectMapper mapper = new CustomJacksonObjectMapper();
            Log.d("TE[GCM]", "resposne success");

            try {
                updateResponse = mapper.readValue(response.getdownloadResponse(), MovieStatusResponse.class);
            } catch (JsonParseException e) {
                updateResponse.setStatus(ProjectConstants.STATUS_ERROR);
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                updateResponse.setSuccess(false);
                Log.d("TE[GCM]", "JsonParseException");

            } catch (JsonMappingException e) {
                updateResponse.setStatus(ProjectConstants.STATUS_ERROR);
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                updateResponse.setSuccess(false);
                Log.d("TE[GCM]", "JsonMappingException");

            } catch (IOException e) {
                updateResponse.setStatus(ProjectConstants.STATUS_ERROR);
                Logger.log(TAG, "Parsing Fail :" + e.getMessage(), LogLevel.LOGGING_LEVEL_ERROR);
                updateResponse.setSuccess(false);
                Log.d("TE[GCM]", "IOException");
            }
        } else {
            Log.d("TE[GCM]", "resposne fail");

            updateResponse.setStatus(ProjectConstants.STATUS_ERROR);
            updateResponse.setSuccess(false);
        }

        if(!updateResponse.isSuccess()) {
            if(updatePaymentFailCount < 5) {
                updatePaymentFailCount++;
                updateMovieStatusRequest(context, movieId, languageId, movieStatus);
            } else {
                updatePaymentFailCount = 0;
                DBStore.getInstance(context).addUpdateFailRequestInDB(movieId,builder.toString());
            }
        }
        return updateResponse;
    }

}