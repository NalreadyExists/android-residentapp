package com.coho.resident.response;

import java.util.HashMap;

public class APIResponse {

	public final static int API_RESPONSE_SUCCESS = 0;
	public final static int API_RESPONSE_FAILURE = 1;

	public final static int ERROR_CODE_NO_NETWORK_ERROR = 2;
	public final static int ERROR_WHILE_SAVING_IN_DB = 3;
	
	
	public final static int API_RESPONSE_INVALID_JSON_METHOD = 500;
	public final static int API_RESPONSE_UNKNOWN_ERROR = 503;
	public final static int API_RESPONSE_ERROR_PARSING_ERROR = 504;
	public final static int API_RESPONSE_BACKEND_COMMUNICATION = 506;
	public final static int API_RESPONSE_LICENCE_FAILURE = 514;
	
	public HashMap<String,String> respHeaders = new HashMap<String, String>();

	public HashMap<String, String> getRespHeaders() {
		return respHeaders;
	}

	public void setRespHeaders(HashMap<String, String> respHeaders) {
		this.respHeaders = respHeaders;
	}

	private int errorCode;
	private String errorMessage;
	private int responseCode = API_RESPONSE_FAILURE;

	public int getResponseCode() {
		return responseCode;
	}

	public APIResponse() {
		this.responseCode = API_RESPONSE_FAILURE;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	@Override
	public String toString() {
		return "APIResponse [errorCode=" + errorCode + ", errorMessage="
				+ errorMessage + ", responseCode=" + responseCode + "]";
	}
	
	

}
