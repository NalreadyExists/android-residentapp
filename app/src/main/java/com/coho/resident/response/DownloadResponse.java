package com.coho.resident.response;

public class DownloadResponse extends APIResponse {

	private String downloadResponse;

	public String getdownloadResponse() {
		return downloadResponse;
	}

	public void setdownloadResponse(String downloadResponse) {
		this.downloadResponse = downloadResponse;
	}

}
