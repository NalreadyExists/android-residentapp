package com.coho.resident.Utilities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.coho.resident.R;
import com.coho.resident.activities.OfferNextNew;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by SAMAR on 8-12-2015.
 */
public class DisplayOfferListAdapterNew extends BaseAdapter {
    Activity activity;
    ArrayList<JSONObject> jsonObject;
    int pos;
    JSONObject j;


    public DisplayOfferListAdapterNew(Activity activity, int i, ArrayList<JSONObject> jsonObject) {
        this.activity = activity;
        this.jsonObject = jsonObject;
        pos = i;
    }


    @Override
    public int getCount() {

        return jsonObject.size();

    }

    @Override
    public Object getItem(int position) {

        return jsonObject.get(position);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View arg1, ViewGroup arg2) {
        LayoutInflater inflater = activity.getLayoutInflater();


        //inflate xml
        View rowView = inflater.inflate(R.layout.activity_offer, null, true);


        LinearLayout layout_offer = (LinearLayout) rowView.findViewById(R.id.linear_layout_offer);
        ImageView img = (ImageView) rowView.findViewById(R.id.logo);
        TextView offer = (TextView) rowView.findViewById(R.id.offer);
        TextView howTo = (TextView) rowView.findViewById(R.id.how_to_avail);

        Button readMore = (Button) rowView.findViewById(R.id.read_more);

        Button website = (Button) rowView.findViewById(R.id.website);

        j = jsonObject.get(position);

        try {

            offer.setText(j.getString("offer"));
            howTo.setText(j.getString("how_to_avail"));
            Ion.with(img)
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.app_icon)
                    .load(j.getString("logo"));


            readMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, OfferNextNew.class);
                    intent.putExtra("json", jsonObject.get(position).toString());
                    activity.startActivity(intent);
                }
            });

            layout_offer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(activity, OfferNextNew.class);
                    intent.putExtra("json", jsonObject.get(position).toString());
                    activity.startActivity(intent);
                }
            });

            website.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    try {
                        intent.setData(Uri.parse(j.getString("website")+ Constants.COHO_UTM_RESIDENT) );
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    activity.startActivity(intent);

                }
            });

        } catch (JSONException e) {

            e.printStackTrace();

        }




        return rowView;
    }


}
