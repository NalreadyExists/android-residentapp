package com.coho.resident.Utilities;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.coho.resident.R;

import java.util.ArrayList;

/**
 * Created by Samar
 */
public class NotificationArrayAdapter extends BaseAdapter {
    Activity activity;
    ArrayList<String> list;
    LayoutInflater inflater;

    public NotificationArrayAdapter(Activity a, ArrayList<String> arr) {
        activity = a;
        list = arr;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.notification_item, null, true);


        TextView tv = (TextView) rowView.findViewById(R.id.notification_content);
        TextView tv1 = (TextView) rowView.findViewById(R.id.category_tv);
        TextView tv2 = (TextView) rowView.findViewById(R.id.date_tv);
        String content = list.get(i);
        String[] a;
        a = content.split("--");


        tv.setText(a[2]);
        tv1.setText(a[1]);
        tv2.setText(a[0]);
        return rowView;

    }
}
