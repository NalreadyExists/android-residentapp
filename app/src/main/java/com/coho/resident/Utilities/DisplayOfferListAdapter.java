package com.coho.resident.Utilities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


import com.coho.resident.R;
import com.coho.resident.activities.OfferNext;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;

/**
 * Created by SAMAR on 8-12-2015.
 */
public class DisplayOfferListAdapter extends BaseAdapter {
    int memClass;
    int cacheSize;
    LruCache cache;
    Activity activity;
    String[] phoneArray;
    String[] category = {"Food", "Health", "Entertainment", "Utilities"};
    ArrayList<String> imgUrlList, key, link;
    int pos;


    public DisplayOfferListAdapter(Activity activity, String[] num, ArrayList<String> a, ArrayList<String> b, int i, ArrayList<String> link) {
        this.activity = activity;
        phoneArray = num;
        this.link = link;
        imgUrlList = a;
        key = b;

        pos = i;
    }


    @Override
    public int getCount() {

        return phoneArray.length;

    }

    @Override
    public Object getItem(int arg0) {

        return null;
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    @Override
    public View getView(final int position, View arg1, ViewGroup arg2) {
        LayoutInflater inflater = activity.getLayoutInflater();


        //inflate xml
        View rowView = inflater.inflate(R.layout.display_offer_item, null, true);

        Log.e("abcd",link.toString()      );

        TextView categoryName = (TextView) rowView.findViewById(R.id.phone_num);
        categoryName.setText("Call" );
        categoryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + phoneArray[position]));
                activity.startActivity(intent);
            }
        });
        /*LinearLayout ll = (LinearLayout) rowView.findViewById(R.id.call_ll);
        ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + phoneArray[position]));
                activity.startActivity(intent);
            }
        });*/

        TextView txt = (TextView) rowView.findViewById(R.id.link);
        txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(link.get(position)));
                activity.startActivity(intent);
            }
        });
        ImageView img = (ImageView) rowView.findViewById(R.id.flyer);

        ImageButton imageButton = (ImageButton) rowView.findViewById(R.id.imageButton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + phoneArray[position]));
                activity.startActivity(intent);
            }
        });


//        final String imageKey = "flyer" + category[pos].toLowerCase() + key.get(position);
        Ion.with(img)
                .placeholder(R.drawable.loading)
                .error(R.drawable.app_icon)
                .load(imgUrlList.get(position));


        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO: 12/8/2015  uncomment
                Intent intent = new Intent(activity, OfferNext.class);
                intent.putExtra("key", imgUrlList.get(position));
                intent.putExtra("phone", phoneArray[position]);
                activity.startActivity(intent);

            }
        });


        return rowView;
    }


}
