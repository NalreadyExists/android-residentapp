package com.coho.resident.Utilities;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.coho.resident.R;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;

/**
 * Created by Samar.
 */
public class CohoAdvantageListAdapter extends BaseAdapter {
    Activity activity;
    //    String[] category = {"Food & Beverages", "Health & Wellness", "Entertainment", "Utility Service"};
    ArrayList<String> image, var;


    public CohoAdvantageListAdapter(Activity activity, ArrayList<String> image, ArrayList<String> var) {
        this.activity=activity;
        this.image = image;
        this.var = var;


    }

    @Override
    public int getCount() {
        int length = image.size();

        return length;

    }
    @Override
    public Object getItem(int arg0) {

        return null;
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }
    @Override
    public View getView(int position, View arg1, ViewGroup arg2) {
        LayoutInflater inflater = activity.getLayoutInflater();


        //inflate xml
        View rowView = inflater.inflate(R.layout.coho_advantage_list_item1, null, true);

        String url = image.get(position);
        ImageView image1 = (ImageView) rowView.findViewById(R.id.image);
        Ion.with(image1)
                .placeholder(R.drawable.loading)
                .load(url);

        TextView categoryName=(TextView) rowView.findViewById(R.id.category_name);
        categoryName.setText(var.get(position));


        return rowView;
    }


}
