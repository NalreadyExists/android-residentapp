package com.coho.resident.Utilities;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/*import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;*/
import com.coho.resident.R;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Sam on 19-08-2015.
 */
public class NotificationListAdapter extends BaseAdapter {
    private ArrayList<String> imageAr, heading, desc, dateAr;
    private ArrayList<JSONObject> jsonObjects;
    Activity activity;
    private LayoutInflater layoutInflater;

    Context context;

    public NotificationListAdapter(Activity activity, ArrayList<String> imageAr, ArrayList<String> desc, ArrayList<String> heading, ArrayList<String> dateAr, ArrayList<JSONObject> jsonObjects) {
        this.imageAr = imageAr;
        this.activity = activity;
        this.heading = heading;
        this.desc = desc;
        this.dateAr = dateAr;
        this.jsonObjects = jsonObjects;
        this.context = context;
    }

    @Override
    public int getCount() {
        return imageAr.size();
    }

    @Override
    public Object getItem(int position) {
        return imageAr.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View arg1, ViewGroup arg2) {
        LayoutInflater inflater = activity.getLayoutInflater();


        //inflate xml
        View rowView = inflater.inflate(R.layout.complaints_list_layout, null, true);

        String url = imageAr.get(position);
        ImageView image1 = (ImageView) rowView.findViewById(R.id.image);
        Ion.with(image1)
                .placeholder(R.drawable.app_icon)
                .error(R.drawable.app_icon)
                .load(url);

        JSONObject j = jsonObjects.get(position);

        try {
            String type = j.getString("type");
            if (type.equals("")) {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        TextView txtheading = (TextView) rowView.findViewById(R.id.heading);
        txtheading.setText(heading.get(position));

        TextView des = (TextView) rowView.findViewById(R.id.description);
        des.setText(desc.get(position));

        TextView datetxt = (TextView) rowView.findViewById(R.id.date);
        datetxt.setText(dateAr.get(position));


        return rowView;
    }
}


