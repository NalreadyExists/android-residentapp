package com.coho.resident.Domain;

import java.io.Serializable;

/**
 * Created by Samar on 12/30/2015.
 */

/*      {
        "type": "comment",
        "title": "Food was great - Romil\n",
        "image": "",
        "category": "Food",
        "description": ""
        }*/

public class Complaint implements Serializable {
    String type, title, image, category, description;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
