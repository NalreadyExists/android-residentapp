package com.coho.resident.Services;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 * Created by samar on 12/4/2015
 */
@SuppressWarnings("deprecation")
public class SessionControl {
    static private HttpClient httpclient = null;

    public static HttpClient getHttpclient() {
        if (httpclient == null) {
            SessionControl.setHttpclient(new DefaultHttpClient());
        }
        return httpclient;
    }

    public static void setHttpclient(HttpClient httpclient) {
        SessionControl.httpclient = httpclient;
    }

}