package com.coho.resident.Services;


import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

@SuppressWarnings("deprecation")
public class JSONParser {
    public JSONObject getJSONData(String url) {
        InputStream inputStream;
        JSONObject responseJson = null;

        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(url);
        // replace with your url

        HttpResponse response;
        try {

            response = client.execute(request);


            if (response != null) {
                inputStream = response.getEntity().getContent();
                try {
                    responseJson = new JSONObject(getJSON_String(inputStream));
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }



            }
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return responseJson;

    }

    private String getJSON_String(InputStream iStream) {
        StringBuffer sb = new StringBuffer();
        char data[] = new char[1024];
        int current = 0;
        InputStreamReader isReader = new InputStreamReader(iStream);
        try {
            while ((current = isReader.read(data, 0, 1024)) != -1) {
                sb.append(data, 0, current);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new String(sb);
    }
}
